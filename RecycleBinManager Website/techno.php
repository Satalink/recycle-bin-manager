<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>techno.mlo@gmail.com</title>
    </head>
    <body>
        <?php
             $error = false;
             $errMsg = "";
             $firstname = isset($_POST['firstname']) ? trim($_POST['firstname']) : '';
             $lastname  = isset($_POST['firstname']) ? trim($_POST['lastname']) : '';
             $birthday_day  = isset($_POST['birthday_day']) ? trim($_POST['birthday_day']) : '';
             $birthday_month  = isset($_POST['birthday_month']) ? trim($_POST['birthday_month']) : '';
             $birthday_year  = isset($_POST['birthday_year']) ? trim($_POST['birthday_year']) : '';
             $favoritefood = isset($_POST['favoritefood']) ? trim($_POST['favoritefood']) : '';
             if(!$firstname&&!$lastname&&!$birthday_day&&!$birthday_month&&!$birthday_year&&!$favoritefood){
                 $error=true;  //First time visit..
             } else {
                //FirstName invalid and not empty
                 if(!preg_match("/^[a-z]{".strlen($firstname)."}$/i", $firstname)||!$firstname){
                     echo '<center><font color="red">First Name</font> is required and may only contain letters.</center><BR>';
                     $error=true;
                 }
                //LastName invalid and not empty
                 if(!preg_match("/^[a-z]{".strlen($lastname)."}$/i", $lastname)||!$lastname){
                     echo '<center><font color="red">Last Name</font> is required and may only contain letters.</center><BR>';
                     $error=true;
                 }
             }

             if ($error) {
                echo '<center>';
                echo '<form action="techno.php" method="POST">';
 		echo '<table border="0" width="55%">';
                //Names
                echo '<tr><td align="right" width="33%">'.requiredField().'First Name:</td><td width="67%"><input type="text" name="firstname" size="27" value="'.$firstname.'"></td></tr>';
                echo '<tr><td align="right" width="33%">'.requiredField().'Last Name:</td><td width="67%"><input type="text" name="lastname" size="27" value="'.$lastname.'"></td></tr>';
                //Birthday
                $months = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
                echo '<tr><td align="right" width="33%">'.requiredField().'Birthday:</td>';
                echo '<td width="67%">';
                echo '<select name="birthday_month">';
                    //Populate Months in Pulldown Menu retain last if error
                    for($m=0;$m<sizeof($months);$m++){
                        echo '<option value="'.($m+1).'"';
                        if($months[$m]==$birthday_month)echo ' selected="selected"';
                        echo '>'.$months[$m];
                        echo '</option>';
                    }
                echo ' </select>';
                echo '<select name="birthday_day">';
                    //Populate Days in Pulldown Menu retain last if error
                    for($d=1;$d<=31;$d++){
                        echo '<option value="'.$d.'"';
                        if($d==$birthday_day)echo ' selected="selected"';
                        echo '>'.$d;
                        echo '</option>';
                    }
                echo ' </select>';
                    //Populate Year in Pulldown Menu retain last if error
                echo '<select name="birthday_year">';
                    $lifespan = 124;  //Oldest person ever surfing web.
                    $thisyear = strftime("%Y",time());
                    for($y=$thisyear;$y>=($thisyear-$lifespan);$y--){
                        echo '<option value="'.$y.'"';
                        if($y==$birthday_year)echo ' selected="selected"';
                        echo '>'.$y;
                        echo '</option>';
                    }
                echo '</select></td></tr>';
                //Favorit Foods
                $foods = array('caret', 'apple', 'steak', 'cracker');
                echo '<tr><td align="right" width="33%">'.requiredField().'Favorite Food:</td>';
                    echo '<td width="67%">';
                    echo '<select name="favorite_food">';
                    //Populate Foods in Pulldown Menu retain last if error
                    for($f=0;$f<sizeof($foods);$f++){
                        echo '<option value="'.$foods[$f].'"';
                        if($foods[$f]==$favorite_food)echo ' selected="selected"';
                        echo '>'.$foods[$f];
                        echo '</option>';
                    }
                    echo ' </select></td></tr>';
		echo '<tr><td align="right"><input type="submit" name="SubmitForm" value="Submit"></td>';
                echo '<td width="67%">'.requiredField().' = required fields.</td></tr>';
                echo '</table>';
                echo '</form>';
                echo '</center>';
             } else {
                $message = '<font color="blue">'.getReverseName($firstname, $lastname).'</font>, you\'re next birthday falls on a <font color="orange">'.getNextBirthdayDay($birthday_month, $birthday_day).'</font>.<BR>May you have <font color="orange">'.$favorite_food.'</font> cake on your special day!<BR><BR><a href="http://www.satalinksoft.com/examples/techno.php">SatalinkSoft PHP example</a>.';
                echo '<center>'.$message.'</center>';
             }

             function requiredField(){
                 return ('<font color="red">*</font>');
             }
             function getReverseName($fname, $lname){
                 $lastArray = str_split($lname);
                 $firstArray = str_split($fname);
                 $namechar_reversed = "";
                 for($lr=sizeof($lastArray);$lr>=0;$lr--){
                     $namechar_reversed .= $lastArray[$lr];
                 }
                 $namechar_reversed .= " ";
                 for($fr=sizeof($firstArray);$fr>=0;$fr--){
                     $namechar_reversed .= $firstArray[$fr];
                 }
                 return($namechar_reversed);
             }
             function getNextBirthdayDay($month, $day){
                 $current_time = time();
                 $thisyear = strftime("%Y",time());
                 for($i=$thisyear;$i<=($thisyear+1);$i++){
                     $birthday_time = mktime(0, 0, 0, $month, $day, $i);
                     if($birthday_time > $current_time){
                        return(date('l', $birthday_time));
                     }
                 }
             }
        ?>
    </body>
</html>
