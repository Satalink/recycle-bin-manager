
package recycle.bin.manager;

public class DBLog {
    Interface iface;
    javax.swing.text.Style[] styles;
    boolean logcleaned = false;
    public DBLog(Interface face){
        iface=face;face=null;
        styles = new javax.swing.text.Style[17];
    }
    public static java.io.OutputStream DisableLOG(){
         return new java.io.OutputStream() {
                @Override
             public void write(int b) throws java.io.IOException {
                 // Ignore all log messages
             }
         };
    }
    public java.util.ArrayList getLog(int logLevel, int limit){
        java.util.ArrayList logList = new java.util.ArrayList<java.util.Map>();
        if(iface.getDB().doesFieldExist("APP.LOG", "ENTRY")){
            String sql = "SELECT * FROM (SELECT ROW_NUMBER() OVER() AS rownum, APP.LOG.* FROM APP.LOG) AS tmp WHERE rownum >= "+
                    (iface.getDB().selectCount("APP.LOG", "ENTRY")-limit) +
                    "AND LEVEL <= "+logLevel+ "ORDER BY TIMESTAMP DESC";
            logList = iface.getDB().Select(sql);
        }
        return(logList);
    }

    public boolean putLog(Object[] logObjs){
        String entry = logObjs[0].toString();
        int style = Integer.valueOf(logObjs[1].toString());
        int level = Integer.valueOf(logObjs[2].toString());
        if(level == 0||level > Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.LOGLEVEL","0").toString())) return(true);
        String sql = "INSERT INTO APP.LOG VALUES('"+entry+"',"+style+","+level+",CURRENT_TIMESTAMP)";
        boolean status = iface.getDB().Statement(sql);
        return(status);
    }
    public boolean putLog(String entry, int style, int level){
        Object[] logObj = new Object[]{entry,style,level};
        return(putLog(logObj));
    }
    public void cleanLog(){
        final int loglevel = Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.LOGLEVEL", "0"));
        final int unit = Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.LOGLEVELUNITINDEX", "1"));
        final int value = Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.LOGLEVELRETENSION", "24"));
        if(logcleaned||loglevel < 1||unit < 1)return;
        Thread thread = new Thread("RecycleBin Clean Lot Thread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                long factor = (3600000);//1 Hour
                switch(unit){
                    case 0: factor=(value);break;
                    case 1: factor=(value*factor);break;
                    case 2: factor=(value*unit*24*factor);break;
                    case 3: factor=(value*unit*24*30*factor);break;
                    default: factor = 0;
                }
                factor = (System.currentTimeMillis() - factor);
                if(loglevel > 0 && unit > 0) {
                    iface.getDB().Statement("DELETE FROM APP.LOG WHERE \"TIMESTAMP\" < '"+iface.getSystem().getTimeStamp(factor)+"' AND LEVEL <= "+loglevel);
                } else if(unit == 0){
                    String sql = "DELETE FROM APP.LOG WHERE NOT EXISTS(SELECT * FROM (SELECT ROW_NUMBER() OVER() AS rownum, APP.LOG.* FROM APP.LOG) AS tmp WHERE rownum >= "+
                    (iface.getDB().selectCount("APP.LOG", "ENTRY")-factor)+")";
                } else {
                    iface.getDB().Statement("DELETE FROM APP.LOG WHERE \"TIMESTAMP\" < '"+iface.getSystem().getCalendarDate(System.currentTimeMillis())+"'");
                }
                logcleaned = true;
            }
        };
        int[] threadPriorities = {2,5,7};
        int threadPriority = threadPriorities[Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.SECUREDELTEPRIORITY", "0"))];
        thread.setPriority(threadPriority);
        thread.setDaemon(true);
        thread.start();
    }
}
