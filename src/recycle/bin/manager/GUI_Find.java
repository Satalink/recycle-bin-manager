package recycle.bin.manager;

public class GUI_Find extends javax.swing.JFrame {
    Interface iface;
    int last;
    Sys system;

    public GUI_Find(Interface face) {
        iface = face; face = null;
        system = iface.getSystem();
        setIconImage(new javax.swing.ImageIcon(system.getResource("/resources/magnify-16.png")).getImage());
        setTitle("Find File");
        last = 0;
        initComponents();
        int guiLocX = iface.getGUI().getLocation().x;
        int guiLocY = iface.getGUI().getLocation().y;
        int guiDimW = iface.getGUI().getSize().width;
        int guiDimH = iface.getGUI().getSize().height;
        int optDimW = getSize().width;
        int optDimH = getSize().height;
        int optLocX = (guiLocX + (guiDimW/2) - (optDimW/2));
        int optLocY = (guiLocY + (guiDimH/2) - (optDimH/2));
        setLocation(optLocX, optLocY);
        setVisible(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel_SearchString = new javax.swing.JLabel();
        jTextField_SearchString = new javax.swing.JTextField();
        jCheckBox_Case = new javax.swing.JCheckBox();
        jButton_Prev = new javax.swing.JButton();
        jButton_Next = new javax.swing.JButton();
        jCheckBox_Match = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Find Recycled File");
        setAlwaysOnTop(true);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setLocationByPlatform(true);
        setName("FindBox"); // NOI18N
        setResizable(false);

        jLabel_SearchString.setText("Find:");

        jTextField_SearchString.setBackground(new java.awt.Color(255, 255, 153));
        jTextField_SearchString.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_SearchStringActionPerformed(evt);
            }
        });

        jCheckBox_Case.setText("Case");

        jButton_Prev.setText("Prev");
        jButton_Prev.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_PrevActionPerformed(evt);
            }
        });

        jButton_Next.setText("Next");
        jButton_Next.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_NextActionPerformed(evt);
            }
        });

        jCheckBox_Match.setText("RegExp");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jCheckBox_Case)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jCheckBox_Match)
                        .addGap(18, 18, 18)
                        .addComponent(jButton_Prev)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton_Next)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel_SearchString)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextField_SearchString, javax.swing.GroupLayout.DEFAULT_SIZE, 246, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_SearchString)
                    .addComponent(jTextField_SearchString, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton_Next)
                    .addComponent(jButton_Prev)
                    .addComponent(jCheckBox_Case)
                    .addComponent(jCheckBox_Match))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextField_SearchStringActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_SearchStringActionPerformed
        last = 0;
        forsearch();
    }//GEN-LAST:event_jTextField_SearchStringActionPerformed

    private void jButton_NextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_NextActionPerformed
        forsearch();
    }//GEN-LAST:event_jButton_NextActionPerformed

    private void jButton_PrevActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_PrevActionPerformed
        revsearch();
    }//GEN-LAST:event_jButton_PrevActionPerformed


    private boolean search(javax.swing.JTable table, String searchText, int i){
            java.util.Map dMap = (java.util.HashMap)table.getValueAt(table.convertRowIndexToModel(i), 0);
            String filename = dMap.get("FileName").toString();
            boolean isFound = false;
            if(!jCheckBox_Case.isSelected()) {
                filename = filename.toLowerCase();
                searchText = searchText.toLowerCase();
            }
            if(jCheckBox_Match.isSelected()) {
                //Convert patterns
                String searchPattern = forRegex(searchText.replaceAll("", ""));
                java.util.regex.Pattern pattern = java.util.regex.Pattern.compile(searchPattern);
                java.util.regex.Matcher matcher = pattern.matcher(filename);
                isFound = matcher.matches();
if(isFound) System.out.println(searchText+" - "+filename+" "+isFound);
            } else {
                if(filename.contains(searchText) && !jCheckBox_Match.isSelected()) {
                    isFound = true;
                } else if (filename.equals(searchText) && jCheckBox_Match.isSelected()) {
                    isFound = true;
                }
            }
            if(isFound) {
                table.setRowSelectionInterval(table.convertRowIndexToView(i), table.convertRowIndexToModel(i));
                iface.getGUI().setTableSelectionViewable();
                last = i;
            }
            return(isFound);
    }


    private void forsearch(){
        String searchText = jTextField_SearchString.getText();
        javax.swing.JTable table = iface.getGUI().getRecycleBinTable();
        if(last < -1) last = -1;
        for(int i=last+1;i < table.getRowCount();i++){
            if(search(table, searchText, i)) break;
        }
    }

    private void revsearch(){
        String searchText = jTextField_SearchString.getText();
        javax.swing.JTable table = iface.getGUI().getRecycleBinTable();
        if(last > table.getRowCount()+1) last = table.getRowCount();
        for(int i=last-1;i > 0;i--){
            if(search(table, searchText, i)) break;
        }
    }

public static String forRegex(String regExpression){
    final StringBuilder result = new StringBuilder();
    final java.text.StringCharacterIterator iterator = new java.text.StringCharacterIterator(regExpression);
    char character =  iterator.current();
    while (character != java.text.CharacterIterator.DONE ){
      /*
      * All literals need to have backslashes doubled.
      */
      if (character == '.') {
        result.append("\\.");
      }
      else if (character == '\\') {
        result.append("\\\\");
      }
      else if (character == '?') {
        result.append("\\?");
      }
      else if (character == '*') {
        result.append("\\*");
      }
      else if (character == '+') {
        result.append("\\+");
      }
      else if (character == '&') {
        result.append("\\&");
      }
      else if (character == ':') {
        result.append("\\:");
      }
      else if (character == '{') {
        result.append("\\{");
      }
      else if (character == '}') {
        result.append("\\}");
      }
      else if (character == '[') {
        result.append("\\[");
      }
      else if (character == ']') {
        result.append("\\]");
      }
      else if (character == '(') {
        result.append("\\(");
      }
      else if (character == ')') {
        result.append("\\)");
      }
      else if (character == '^') {
        result.append("\\^");
      }
      else if (character == '$') {
        result.append("\\$");
      }
      else {
        //the char is not a special one
        //add it to the result as is
        result.append(character);
      }
      character = iterator.next();
    }
    return result.toString();
  }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_Next;
    private javax.swing.JButton jButton_Prev;
    private javax.swing.JCheckBox jCheckBox_Case;
    private javax.swing.JCheckBox jCheckBox_Match;
    private javax.swing.JLabel jLabel_SearchString;
    private javax.swing.JTextField jTextField_SearchString;
    // End of variables declaration//GEN-END:variables

}
