package recycle.bin.manager;

import at.jta.RegistryErrorException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Sys {
    java.util.Map fontMap = new java.util.HashMap<String, java.awt.Font>();
    java.util.Map colorMap = new java.util.HashMap<String, java.awt.Color>();
    private java.util.Map<String, javax.swing.Icon> iconMap = new java.util.HashMap<String, javax.swing.Icon>();
    private javax.swing.JFileChooser fc = new javax.swing.JFileChooser();
    private Interface iface;
    public Sys(Interface face){
        iface = face;face=null;
        setColorMap();
        System.out.println("System OK.");
    }
//Internal
    public boolean getNetStatus() {
        boolean status = true;
        try {
            java.net.URL url = new java.net.URL("http://www.satalinksoft.com");
            java.net.URLConnection urlConn = url.openConnection();
        } catch (IOException ex1) {
            status = false;
        }
        return status;
    }
    public String getJavaVersion() {
        return(System.getProperty("java.version"));
    }
    public String getUserHome() {
        return(System.getProperty("user.home"));
    }
    public String getUserName() {
        return(System.getProperty("user.name"));
    }
    public String getOS() {
        return(System.getProperty("os.name"));
    }
    public long genRandomLong(long x){
        java.util.Random random = new java.util.Random();
        return(random.nextLong());
    }
    public int genRandomInt(int x){
        java.util.Random random = new java.util.Random();
        return(random.nextInt(x));
    }
    public int getPercentComplete(int a, int b) {
        int pct = (a*100/b);
        return(pct);
    }
    public java.net.URL getResource(String reference) {
        return(Class.class.getResource(reference));
    }
    public String getHostname() {
        String hostname = null;
                java.net.InetAddress addr = null;
            try {
                addr = java.net.InetAddress.getLocalHost();
            } catch (java.net.UnknownHostException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        hostname = addr.getHostName();
        return(hostname);
    }
    public String getIPaddress() {
        String ipAddr = "Unknown";
        try {
            ipAddr = java.net.InetAddress.getLocalHost().getHostAddress();
        } catch (java.net.UnknownHostException ex) {
        }
        return(ipAddr);
    }
    public void execCMD(String cmd) {
        try {
            Process process = Runtime.getRuntime().exec(cmd);
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    public void launchBrowser(String url) {
        if(java.awt.Desktop.isDesktopSupported()){
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            try {
                desktop.browse(java.net.URI.create(url));
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }
    public void sendMail(String url){
        if(java.awt.Desktop.isDesktopSupported()){
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            try {
                desktop.mail(java.net.URI.create(url));
            } catch (java.io.IOException ex){
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }
    public void editFile(java.io.File file){
        if(java.awt.Desktop.isDesktopSupported()){
            java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
            try {
                desktop.edit(file);
            } catch (java.io.IOException ex){
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
    }
//Time
    public String getDateStamp(){
        java.util.Calendar timestamp = new java.util.GregorianCalendar();
        java.text.NumberFormat intformat = new java.text.DecimalFormat("00");
        String DD = intformat.format(timestamp.get(java.util.Calendar.DAY_OF_MONTH));
        String MM = intformat.format(timestamp.get(java.util.Calendar.MONTH) + 1);
        String YY = intformat.format(timestamp.get(java.util.Calendar.YEAR));
        String dateStamp = YY+MM+DD;
        return(dateStamp);
    }
    public Long getEpoch() {
        long epoch = java.util.Calendar.getInstance().getTimeInMillis();
        return(epoch);
    }
    public long getEpoch(String datetime) {
        try {
            long epoch = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datetime).getTime();
            return epoch;
        } catch (java.text.ParseException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            return(-1);
        }
    }
    public String getTimeStamp() {
        String timestamp = new java.sql.Timestamp(getEpoch()).toString();
        return(timestamp);
    }
    public String getTimeStamp(long epoch) {
        String timestamp = new java.sql.Timestamp(epoch).toString();
        return(timestamp);
    }
    public String getTimeStamp_YY_MM_DD_hh_mm_ss(){
        java.util.Calendar timestamp = new java.util.GregorianCalendar();
        java.text.NumberFormat intformat = new java.text.DecimalFormat("00");
        String DD = intformat.format(timestamp.get(java.util.Calendar.DAY_OF_MONTH));
        String MM = intformat.format(timestamp.get(java.util.Calendar.MONTH) + 1);
        String YY = intformat.format(timestamp.get(java.util.Calendar.YEAR));
        String hh = intformat.format(timestamp.get(java.util.Calendar.HOUR_OF_DAY));
        String mm = intformat.format(timestamp.get(java.util.Calendar.MINUTE));
        String ss = intformat.format(timestamp.get(java.util.Calendar.SECOND));
        String timeStamp = YY+"-"+MM+"-"+DD+" "+hh+":"+mm+":"+ss;
        return(timeStamp);
    }
    public String getCalendarDate(long longtime) {
                java.util.Date mtime = new java.util.Date(longtime);
                String timestring = java.text.DateFormat.getDateInstance(java.text.DateFormat.MEDIUM).format(mtime)+" "+java.text.DateFormat.getTimeInstance(java.text.DateFormat.SHORT).format(mtime);
                return(timestring);
    }

//File
    public String getFileExtension(java.io.File file) {
        String[] pts = file.getName().split("\\.");
        String ext = pts[(pts.length - 1)].toLowerCase();
        return(ext);
    }
    public javax.swing.JFileChooser getFileChooser() {
        return(fc);
    }
    public java.util.ArrayList getFileOpen(java.io.File file){
            java.util.ArrayList fileBufferList = new java.util.ArrayList();
            try {
                if(!file.canRead()) file.setReadable(true);
                java.io.FileInputStream fis = new java.io.FileInputStream(file);
                int bytesize = 1024;
                if(file.length() < Integer.MAX_VALUE){
                    bytesize = Integer.valueOf(String.valueOf(file.length()));
                }
                byte[] buf = new byte[bytesize];
                int i = 0;
                while ((i = fis.read(buf)) != -1) {
                    fileBufferList.add(buf);
                    i--;
                }
                buf=null;
                fis.close();
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            return(fileBufferList);
    }
    public long getFileSize(java.io.File file) {
        long size = 0;
        if(file.isFile()) {
            size = file.length();
        } else if(file.isDirectory()) {
            long tsize = 0;
            java.io.File[] flist = file.listFiles();
            for (int i=0;i < flist.length;i++) {
                java.io.File tfile = new java.io.File(flist[i].getAbsolutePath());
                if(tfile.isFile()) {
                    tsize+=tfile.length();
                } else {
                    tsize+=getFileSize(tfile);
                }
             }
             size = tsize;
        }
        return(size);
    }
    public String getFileSize(long size) {
        String unit = null;
        float product = (float) 0.00;
        java.text.NumberFormat decformat = new java.text.DecimalFormat("0.00");
        unit = " MB";
        product = size;
        String sizeStr = new String(decformat.format(product))+" "+unit;
        return(sizeStr);
    }
    public java.util.Map filePermisions(java.io.File file) {
        java.util.Map perms = null;
        if(file.exists()) {
            perms.put("exists", file.exists());
            perms.put("isDirectory", file.isDirectory());
            perms.put("isHidden", file.isHidden());
            perms.put("canRead", file.canRead());
            perms.put("canWrite", file.canWrite());
            perms.put("canExecute", file.canExecute());
        } else {
            perms.put("exists", file.exists());
        }
        return(perms);
    }
    public java.util.ArrayList getRecursiveFiles(java.io.File file) {
        java.util.ArrayList fileList = new java.util.ArrayList();
        if (file.isDirectory()) {
            String[] children = file.list();
                for (int i=0;i<children.length;i++) {
                    fileList.addAll(getRecursiveFiles(new java.io.File(file, children[i])));
            }
        } else if (file.isFile()) fileList.add(file);
        return(fileList);
    }
    public boolean getClipboardAccess() {
        boolean sma = false;
            SecurityManager sm = System.getSecurityManager();
            try {
              if (sm != null) {
                sm.checkSystemClipboardAccess();
              }
              sma = true;
            } catch (SecurityException ex) {
              sma = false;
            }
        return(sma);
    }
    public java.io.File[] getRecycleBin() throws javax.swing.text.BadLocationException {
        java.io.File[] rbins = new java.io.File[java.io.File.listRoots().length];
        String os = getOS();
        if(os.contains("Windows")) {
            java.io.File[] roots = java.io.File.listRoots();
            for(int i=0;i < roots.length;i++) {
                java.io.File test = null;
                java.io.File[] fileBins = new java.io.File[3];
                fileBins[0] = new java.io.File(roots[i].getAbsolutePath()+"Recycled"+java.io.File.separator);
                fileBins[1] = new java.io.File(roots[i].getAbsolutePath()+"Recycler"+java.io.File.separator);
                fileBins[2] = new java.io.File(roots[i].getAbsolutePath()+"$Recycle.Bin"+java.io.File.separator);
                java.util.Map OpSystems = new java.util.HashMap<String,Integer>();
                    OpSystems.put("Windows 95", 0);
                    OpSystems.put("Windows 98", 0);
                    OpSystems.put("Windows NT", 0);
                    OpSystems.put("Windows 2000", 1);
                    OpSystems.put("Windows 2003", 1);
                    OpSystems.put("Windows XP", 1);
                    OpSystems.put("Windows Vista", 2);
                    OpSystems.put("Windows 7", 2);
                if (OpSystems.containsKey(os)){
                    int osKey = (Integer)OpSystems.get(os);
                    test = fileBins[osKey];
                } else {
                    javax.swing.JOptionPane.showMessageDialog(null, "Your OS "+os+" not found in list.\n\nContact SatalinkSoft Support for assistance.", "ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
                }
                if(test.exists()) {
                    if(new java.io.File(test.getAbsolutePath()+"\\"+getSID()).exists()) {
                       rbins[i] = new java.io.File(test.getAbsolutePath()+"\\"+getSID());
                    } else {
                       rbins[i] = null;
                    }
                } else {
                    rbins[i] = null;
                }
            }
        }
        return(rbins);
    }
    public javax.swing.Icon getSystemFileIcon(java.io.File file) {
        String ext = "";
        if(file.getName().contains(".")) {
            String[] exts = file.getName().split("\\.");
            ext = exts[(exts.length - 1)].toLowerCase();
        }
        java.util.Map exceptionMap = new java.util.HashMap<String, String>();
        exceptionMap.put("exe", "Executable File Icons");
        exceptionMap.put("lnk", "Shortcuts");
        exceptionMap.put("url", "Web Links");
        if(iconMap.containsKey(ext)) {
            return(iconMap.get(ext));
        } else if(iconMap.containsKey(file.getName().toString())){
            return(iconMap.get(file.getName().toString()));
        } else if(exceptionMap.containsKey(ext)) {
            javax.swing.Icon icon = fc.getFileSystemView().getSystemIcon(file);
            iconMap.put(file.getName().toString(), icon);
            return(icon);
        } else {
            javax.swing.Icon icon = fc.getFileSystemView().getSystemIcon(file);
            iconMap.put(ext, icon);
            return(icon);
        }
    }
    public boolean copyFile (java.io.File fromFile, java.io.File toFile) throws java.io.FileNotFoundException {
        boolean status = false;
        if(!fromFile.exists()) return(status);
        try {
            java.io.FileInputStream fis = new java.io.FileInputStream(fromFile);
            java.io.FileOutputStream fos = new java.io.FileOutputStream(toFile);
            byte[] buf = new byte[1024];
            int i = 0;
            while ((i = fis.read(buf)) != -1) {
                fos.write(buf, 0, i);
            }
            buf=null;
            fis.close();
            fos.close();
            if (toFile.exists()) {
            status = true;
            }
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        return(status);
    }
    public boolean moveFile (java.io.File fromFile, java.io.File toFile) {
        boolean status = false;
        fromFile.renameTo(toFile);
        return(status);
    }
    public boolean deleteFile(String filepath, boolean secure, int secLevel, int secType, boolean secDelInfo2, boolean secDelFileNameRnd) {
        boolean status = true;
        java.io.File file = new java.io.File(filepath);
        if (file.isDirectory()) {
            String[] children = file.list();
            for (int i=0; i<children.length; i++) {
                java.io.File dfile = new java.io.File(file.getAbsolutePath()+java.io.File.separator+children[i]);
                if(dfile.exists() && dfile.isFile()) {
                    if(secure) dfile = secureOverwrite(dfile, secLevel, secType, secDelFileNameRnd);
                    status = deleteFile(dfile.getAbsolutePath(), secure, secLevel, secType, secDelInfo2, secDelFileNameRnd);
                    if(iface.getGUI() != null){
                        if(status && iface.getDB().Connect())iface.getDB().RBMlog.putLog(dfile.getAbsolutePath()+" file deleted",1,4);
                    }
                } else if(dfile.exists() && dfile.isDirectory()) {
                    status = deleteFile(dfile.getAbsolutePath(), secure, secLevel, secType, secDelInfo2, secDelFileNameRnd);
                    if(iface.getDB() != null){
                        if(status && iface.getDB().Connect())iface.getDB().RBMlog.putLog(dfile.getAbsolutePath()+" directory deleted",1,4);
                    }
                }
                if(dfile.exists() && !status){
                    if(iface.getDB() != null){
                        if(iface.getDB().Connect())iface.getDB().RBMlog.putLog(dfile.getAbsolutePath()+" is locked, file will be deleted on RBM exit",9,1);
                    }
                    dfile.deleteOnExit();
                }
            }
        }
        if(status) {
            if(!file.canRead()) file.setReadable(true);
            if(!file.canWrite()) file.setWritable(true);
            if(secure) {
                if(!file.canExecute()) file.setExecutable(true);
                file = secureOverwrite(file, secLevel, secType, secDelFileNameRnd);
            }
            status = file.delete();
            if(!status) file.deleteOnExit();
        } else {
            file.deleteOnExit();
        }
        return(status);
    }
    public java.io.File secureOverwrite(java.io.File file, int level, int type, boolean secDelFileNameRnd){
        if(file.exists()){
            if(file.isFile()){
                long stopbyte = file.length();
                try {
                    if(Boolean.valueOf(iface.getAppFunctions().properties.getProperty("APP.SECUREDELETELIMIT", "false"))){
                        int unitindex = Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.SECUREDELETELIMITUNIT", "1"));
                        int limitval = Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.SECUREDELETELIMITSIZE", "10"));
                        int limitpercent = Integer.valueOf(iface.getAppFunctions().properties.getProperty("APP.SECUREDELETELIMITPERCENT", "1"));
                        long basesize = 0;
                        long filesize = file.length();
                        if(unitindex==0){
                            basesize = 1000;
                        } else
                        if(unitindex==1){
                            basesize = 1000000;
                        } else
                        if(unitindex==2){
                            basesize = 1000000000;
                        }
                        long limitsize = (basesize * limitval);
                        if(filesize > limitsize){
                            stopbyte = (long)(file.length() * limitpercent)/100;
                        }
                        if(stopbyte > Integer.MAX_VALUE) stopbyte = (Integer.MAX_VALUE - 1);
                    }
                    java.security.SecureRandom random = new java.security.SecureRandom();
                    java.io.RandomAccessFile raf = new java.io.RandomAccessFile(file, "rw");
                    java.nio.channels.FileChannel channel = raf.getChannel();
                    java.nio.MappedByteBuffer buffer = channel.map(java.nio.channels.FileChannel.MapMode.READ_WRITE, 0, stopbyte);
                    //Levels 0=Single, 1=Double, 2=Triple
                    //Types 0=Random, 1=Zeroize, 2=Initialize, 3=Randomize
                    byte[] data = new byte[1];
                    //Loop through the secure delete levels
                    while(level >= 0){
                        int secType;
                        //Set Security Type
                        switch(type){
                            case 0: secType = genRandomInt(3);break;
                            default: secType = type;break;
                        }
                        //OverWrite Data with Secure Option
                        while(buffer.hasRemaining()){
                            switch(secType){
                                case 1: buffer.put((byte) 0x00);break;
                                case 2: buffer.put((byte) 0xFF);break;
                                case 3: random.nextBytes(data);buffer.put(data);break;
                                default: random.nextBytes(data);buffer.put(data);break;
                            }
                        }
                        //Write OverWrite Data
                        buffer.force();
                        if(level > 0){
                            buffer.rewind();
                        } else {
                            buffer = null;
                        }
                        level--;
                    }
                    channel.close();
                    raf.close();
                    channel = null;
                    raf = null;
                    iface.getDB().RBMlog.putLog(file.getAbsolutePath()+" digitally shredded.",1,3);
                    if(secDelFileNameRnd){
                        String origName = file.getAbsolutePath();
                        String rndName = String.valueOf(genRandomInt(1000000));
                        if(file.renameTo(new java.io.File(file.getParent()+java.io.File.separator+rndName))){
                            file = new java.io.File(file.getParent()+java.io.File.separator+rndName);
                            file.setLastModified(Long.valueOf(genRandomInt(1000000000)));
                            iface.getDB().RBMlog.putLog(origName+" renamed "+file.getAbsolutePath(),1,3);
                        }
                    }
                    System.gc();
                } catch (java.io.IOException ex) {
                    iface.getDB().RBMlog.putLog("ERROR SECURE DELETE: "+stopbyte+"@"+file.getAbsolutePath(),9,1);
                }
            }
        }
        return(file);
    }
    public String getSID() {
        String path = "Software\\Microsoft\\Protected Storage System Provider";
        return(RegTools.getSubKeyByKeyName_HKCU(path, 0, 1024).trim());
    }

    public String getMcxSID(){
        String McxSID = "";
        try {
            //Check For Media Center User
            at.jta.Regor reg = new at.jta.Regor();
            at.jta.Key key = new at.jta.Key();
            key = reg.openKey(reg.HKEY_LOCAL_MACHINE,"SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\ProfileList");
            if(key._isValidKey()){
                java.util.List keyList = reg.listKeys(key);
                //ProfileImagePath Contains Mcx
                at.jta.Key subkey = new at.jta.Key();
                for(int i=0;i<keyList.size();i++){
                    subkey = reg.openKey(reg.HKEY_LOCAL_MACHINE, key.getPath().replace("HKEY_LOCAL_MACHINE\\", "")+"\\"+keyList.get(i));
                    if(subkey._isValidKey()){
                        String pipString = reg.readAnyValueString(subkey, "ProfileImagePath");
                        if(pipString.contains("Mcx")){
                            McxSID=keyList.get(i).toString();
                        }
                    }
                }
            }
        } catch (RegistryErrorException ex) {
            Logger.getLogger(Sys.class.getName()).log(Level.SEVERE, null, ex);
        }
        return(McxSID);
    }
    public boolean fileWriteBytes(java.io.File file, byte[] bytes, boolean append){
        java.io.FileOutputStream fos = null;
        boolean status = false;
        try {
            fos = new java.io.FileOutputStream(file, append);
            fos.write(bytes);
            bytes=null;
        } catch (java.io.IOException ex) {
            java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (java.io.IOException ex) {
                java.util.logging.Logger.getLogger(Sys.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
        }
        return status;
    }

    //Colors & Fonts
    public void setColorMap(){
        colorMap.put("black", new java.awt.Color(0,0,0));
        colorMap.put("white", new java.awt.Color(255,255,255));
        colorMap.put("header", new java.awt.Color(200,200,200));
        colorMap.put("menu", new java.awt.Color(220,220,220));
        colorMap.put("skyblue", new java.awt.Color(135,206,235));
        colorMap.put("cloudwhite", new java.awt.Color(209,238,238));
    }

    public void setFontMap(java.awt.Font font){
        String fontName = font.getFontName();
        int fontSize = font.getSize();
        int style = font.getStyle();
        fontMap.put("normal", new java.awt.Font(fontName, style, fontSize));
        fontMap.put("big", new java.awt.Font(fontName, style, (int)(fontSize * 1.1)));
        fontMap.put("large", new java.awt.Font(fontName, style, (int)(fontSize * 1.2)));
        fontMap.put("giant", new java.awt.Font(fontName, style, (int)(fontSize *1.4)));
        fontMap.put("header", new java.awt.Font(fontName, 1, (int)(fontSize * 1.6)));
    }

    //Process
     public String[] sortArray(String[] strArray) {
       if (strArray.length == 1) return(strArray);   // no need to sort one item
       java.text.Collator collator = java.text.Collator.getInstance();
       String strTemp;
       for (int i=0;i<strArray.length;i++) {
         for (int j=i+1;j<strArray.length;j++) {
           if (collator.compare(strArray[i], strArray[j]) > 0) {
             strTemp = strArray[i];
             strArray[i] = strArray[j];
             strArray[j] = strTemp;
           }
         }
       }
       return(strArray);
     }
     public javax.swing.JList sortList(javax.swing.JList list) {
       javax.swing.ListModel model = list.getModel();
       int listSize = model.getSize();
       String[] array = new String[listSize];
       for (int i=0;i<listSize;i++) {
         array[i] = (String)model.getElementAt(i);
       }
       sortArray(array);
       list.setListData(array);
       list.revalidate();
       return(list);
     }
    
}
