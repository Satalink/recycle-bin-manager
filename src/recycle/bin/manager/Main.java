package recycle.bin.manager;

public class Main implements Interface{
    protected recycle.bin.manager.Sys system;
    protected GUI gui;
    protected AppFunctions func;
    protected DB db;
    @SuppressWarnings("empty-statement")
    public Main() throws javax.swing.text.BadLocationException {
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Main.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
	}

        system = new Sys(this);
        db = new DB(this);
        func = new AppFunctions(this);
        gui = new GUI(this);
        gui.createTableModel(new java.util.ArrayList());
        gui.startContentThread();
    }

    public static void main(String[] args) throws javax.swing.text.BadLocationException {
        Main main = new Main();
    }

    @Override
    public Sys getSystem(){
        return(system);
    }
    @Override
    public AppFunctions getAppFunctions() {
        return(func);
    }
    @Override
    public GUI getGUI() {
        return(gui);
    }
    @Override
    public DB getDB() {
        return(db);
    }
}






