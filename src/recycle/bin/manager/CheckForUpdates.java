package recycle.bin.manager;

public class CheckForUpdates {
    public CheckForUpdates(Interface iface, boolean manual){
        boolean update = false;
        String versionStr = iface.getAppFunctions().VERSION;
        Double version = Double.valueOf(versionStr);
        String program = iface.getAppFunctions().PROGNAME;
        try {
            int dbID = 1;
            MySQL_DB mdb = new MySQL_DB(iface);
            java.util.Map latestMap = (java.util.Map)mdb.Select(1, "SELECT VERSION FROM APP").get(0);
            Double latest = Double.valueOf(latestMap.get("VERSION").toString());
            if(version < latest){
                mdb.Statement(0, "INSERT INTO `LOG` (`ENTRY`,`USER',`TIMESTAMP` VALUES('VERSIONCHECK: "+version+"==>"+latest+"','"+iface.getAppFunctions().user+"@"+iface.getSystem().getHostname()+"',CURRENT_TIMESTAMP)");
                Object[] options = {"Get Update v"+latest, "Continue using v"+version};
                boolean result = iface.getGUI().confirmationDialog(
                        "A more current version of "+program+" is available.",
                        "Update Confirmation",
                        options
                        );
                if(result){
                    mdb.Statement(0, "INSERT INTO `LOG` (`ENTRY`,`USER',`TIMESTAMP` VALUES('VERSIONUPDATE: "+version+"==>"+latest+"','"+iface.getAppFunctions().user+"@"+iface.getSystem().getHostname()+"',CURRENT_TIMESTAMP)");
                    iface.getSystem().launchBrowser("http://www.satalinksoft.com/download/RecycleBinManager_Setup.exe");
                }
            } else if(manual) {
                    Object[] coptions = {"OK", "Cancel"};
                    boolean cresult = iface.getGUI().confirmationDialog(
                        program+" v"+version+" is the latest version.",
                        "Update Notification",
                        coptions
                        );
            }
        } catch (Exception e) {
            if(manual){
                    Object[] coptions = {"OK", "Cancel"};
                    boolean cresult = iface.getGUI().confirmationDialog(
                        "An error occured while checking for updates.",
                        "Recycle Bin Manager: Update Notification",
                        coptions
                        );
            }
        }
    }
}
