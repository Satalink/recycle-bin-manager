
package recycle.bin.manager;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.text.BadLocationException;

public class DB {
    // runtime variables.
    private Interface iface;
    private String DBDriver = "org.apache.derby.jdbc.EmbeddedDriver";
    private String dbUser;
    private String dbPass;
    private String dbProtocol = "jdbc:derby:";
    private String dbURL;
    private Connection conn;
    private boolean status = false;
    java.util.Properties dbProperties = new java.util.Properties();
    public DBLog RBMlog = null;
    public String dbName;

    public DB(Interface face) {
        try {
            iface = face;
            face = null;
            conn = null;
            java.io.File[] bins = iface.getSystem().getRecycleBin();
            java.io.File bin = null;
            for(int i=0;i<bins.length;i++){
                if(bins[i] != null){
                    bin = bins[i].getParentFile();
                    i=bins.length;
                }
            }
            if(bin == null){
                javax.swing.JOptionPane.showMessageDialog(null, "No recycle bin locations found.\nPlease ensure you have the Windows recycle bin function enabled.","ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
            }
            dbName = bin.getAbsolutePath()+"\\S-1-5-20\\";
            dbName = dbName += iface.getSystem().getSID();
            java.io.File dbFile = new java.io.File(dbName);
            if (!dbFile.getParentFile().exists()) {
                dbFile.getParentFile().mkdirs();
            }
            dbUser = iface.getSystem().getUserName().replace(" ", "");
            dbPass = iface.getSystem().getHostname().replace(" ", "");
            dbURL = dbProtocol + dbName;
            dbProperties.put("dataEncryption", "true");
            dbProperties.put("bootPassword", iface.getSystem().getSID().replace("-", ""));
            dbProperties.put("encryptionAlgorithm", "AES/CBC/NoPadding");
            dbProperties.put("logDevice", dbName + "//derby.log");
            System.setProperty("derby.stream.error.method", "recycle.bin.manager.DBLog.DisableLOG");
            Connect();
            System.out.println("Database OK.");
        } catch (BadLocationException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean DBexists() {
        return(new java.io.File(dbName).exists());
    }

    public void DBclose() {
        try {
            if (conn != null) {
                Statement("INSERT INTO APP.LOG VALUES('Database Closed',13,4,CURRENT_TIMESTAMP)");
                conn.close();
                conn = null;
            }
        }
        catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean Connect() {
        try {
            status = false;
            if(conn != null){
                if (conn.isValid(60)) {
                    status = true;
                }
            }
            else if (DBexists()) {
                    try {
                        Class.forName(DBDriver).newInstance();
                        conn = DriverManager.getConnection(dbURL + ";user=" + dbUser + ";password=" + dbPass + ";", dbProperties);
                        status = conn.isValid(60);
                    } catch (Exception ex) {
                        status = false;
                        if(conn != null) conn.close();
                        Object[] options = {"Delete Database", "Open Support Ticket"};
                        String text = "There is a problem connecting to your configuration database.\nYou should contact Satalinksoft Support for help resetting it.\nIf requested, you may need to delete the DB and get a new key.\nWould you like to delete your configuration database?";
                        String title = "Database Error";
                        int n = javax.swing.JOptionPane.showOptionDialog(
                                new javax.swing.JFrame(),
                                text,
                                title,
                                javax.swing.JOptionPane.YES_NO_OPTION,
                                javax.swing.JOptionPane.QUESTION_MESSAGE,
                                null,
                                options,
                                options[1]
                                );
                        if(n==0){
                          boolean delStat = iface.getSystem().deleteFile(dbName, false, 0, 0, false, false);
                            String msg = "Error deleting database";
                            if(delStat){
                                msg = "Database deleted successfully\nRestarting Recycle Bin Manager recreates the database.";
                            }
                            javax.swing.JOptionPane.showMessageDialog(null, msg, "Delete Result:", javax.swing.JOptionPane.PLAIN_MESSAGE, null);
                        } else {
                            iface.getSystem().launchBrowser("http://support.satalinksoft.com");
                        }
                        System.exit(0);
                    }
            } else {
                try {
                    Class.forName(DBDriver).newInstance();
                    conn = DriverManager.getConnection(dbURL + ";create=true;user=" + dbUser + ";password=" + dbPass + ";", dbProperties);
                    status = conn.isValid(60);
                    if (status) {
                        Initialize();
                    }
                } catch (Exception ex) {
                    javax.swing.JOptionPane.showMessageDialog(null, "Recycle Bin Manager needs to be ran as Administrator.\nExplore the shortcut's properties to set privilages.", "Critical Error:", javax.swing.JOptionPane.ERROR_MESSAGE);
                    Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
                    status = false;
                    iface.getAppFunctions().exitApp(status);
                }
            }
            return(status);
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
            return(false);
        }
    }
    public void startLog(){
        RBMlog = new DBLog(iface);
        Object[] logObj = {"Recycle Bin Manager Started","13","4"};
        RBMlog.putLog(logObj);
    }
    protected void Initialize() {
        String sql;

        // Creation of CONFIGS
        sql =
            "CREATE TABLE APP.PROPERTIES ("+
            "PROPERTY VARCHAR(128) not null primary key,"+
            "VALUE VARCHAR(1024)"+
            ")";
        status = Statement(sql);

        // FILE TYPES
        sql =
           "CREATE TABLE APP.FILETYPES ("+
            "NAME VARCHAR(128) not null primary key,"+
            "ENABLED VARCHAR(8),"+
            "RETENSIONUNIT INTEGER,"+
            "RETENSIONVALUE INTEGER,"+
            "SECUREDELETE VARCHAR(8)"+
            ")";
        status = Statement(sql);

        // EXTENSIONS
        sql =
            "CREATE TABLE APP.EXTENSIONS ("+
            "NAME VARCHAR(32) not null primary key,"+
            "FILETYPE VARCHAR(32)"+
            ")";
        status = Statement(sql);

        // LOGGING
        sql =
           "CREATE TABLE APP.LOG ("+
            "ENTRY VARCHAR(512),"+
            "STYLE INTEGER,"+
            "LEVEL INTEGER,"+
            "TIMESTAMP TIMESTAMP"+
            ")";
        status = Statement(sql);
        status = Statement("INSERT INTO APP.LOG VALUES('Database Created',13,4,CURRENT_TIMESTAMP)");
        // FILES
        sql =
            "CREATE TABLE APP.FILES ("+
            "ID INTEGER not null primary key,"+
            "NAME VARCHAR(512),"+
            "PATH VARCHAR(1024),"+
            "RNAME VARCHAR(16),"+
            "RPATH VARCHAR(1024),"+
            "INAME VARCHAR(16),"+
            "DATA BLOB,"+
            "DELETEDON TIMESTAMP,"+
            "SIZE VARCHAR(16)"+
            ")";
          status = Statement(sql);
    }

    @SuppressWarnings("unchecked")
    public ArrayList Select(String sqlstatement) {
        ArrayList<Map> resultList = new ArrayList<Map>();
        try {
            if(conn == null || conn.isClosed()) {
                Connect();
            }
            if(!conn.isClosed()) {
                Statement stmt = conn.createStatement();
                ResultSet resultset = stmt.executeQuery(sqlstatement);
                ResultSetMetaData rsmd = resultset.getMetaData();
                int columns = rsmd.getColumnCount();
                int i = 0;
                while (resultset.next()) {
                    int col = 1;
                    Map resultMap = new HashMap();
                    while (col <= columns) {
                        String value = resultset.getString(col);
                        String colName = rsmd.getColumnName(col);
                        resultMap.put(colName, value);
                        col++;
                    }
                    resultList.add(0, resultMap);
                    i++;
                }
                resultset.close();
                stmt.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultList;
    }

    public int selectCount(String table, String column) {
	int resultvalue = 0;
        String sql = "SELECT COUNT("+column+") from "+table;
	try {
		Statement stmt = conn.createStatement();
		ResultSet results = stmt.executeQuery(sql);
                if(results.next()) {
		    resultvalue = results.getInt(1);
		}
		results.close();
		stmt.close();
	} catch (SQLException ex) {
		resultvalue = 0;
	}
	return(resultvalue);
    }

    public int selectMaxInt(String table, String column) {
        int resultvalue = 0;
	try {
            String sql = "SELECT MAX("+column+") FROM "+table;
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(sql);
            if(results.next()) {
                resultvalue = results.getInt(1);
            }
            results.close();
            stmt.close();
	} catch (SQLException ex) {
	    resultvalue = 0;
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
	}
        return (resultvalue);
    }

    public String selectMaxStr(String table, String column) {
        String resultvalue = null;
	try {
            String sql = "SELECT MAX("+column+") FROM "+table;
            Statement stmt = conn.createStatement();
            ResultSet results = stmt.executeQuery(sql);
            if(results.next()) {
                resultvalue = results.getString(1);
            }
            results.close();
            stmt.close();
	} catch (SQLException ex) {
	    resultvalue = null;
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, null, ex);
	}
        return (resultvalue);
    }


    public boolean clearTable(String table, String key){
        boolean result = false;
        if(selectCount(table,key)>0){
            String sql = "DELETE FROM "+table+" WHERE "+key+" IN ("+
                    "SELECT "+key+" FROM "+table+")";
            result = Statement(sql);
        }
        return(result);
    }

    public boolean doesValueExist(String table, String key, String value){
        boolean exists = false;
        int count = 0;
        try {
            if(selectCount(table,key) > 0){
                String sql = "SELECT COUNT(*) FROM "+table+" WHERE "+key+" = '"+value+"'";
                Statement stmt = conn.createStatement();
                ResultSet result = stmt.executeQuery(sql);
                if(result.next()) {
                    count = result.getInt(1);
                }
                if(count > 0){
                    exists = true;
                }
            }
        } catch (SQLException ex) {
        }
        return(exists);
    }
    public boolean doesFieldExist(String table, String key){
        boolean exists = false;
        int count = 0;
            if(selectCount(table,key) > 0){
               exists = true;
            }
        return(exists);
    }
    public boolean Statement(String sqlstatement) {
        try {
            if(!conn.isValid(60)){
                Connect();
            }
            conn.createStatement().execute(sqlstatement);
            status = true;
        } catch (SQLException ex) {
            Logger.getLogger(DB.class.getName()).log(Level.SEVERE, sqlstatement, ex);
            status = false;
        }
        return(status);
    }
}
