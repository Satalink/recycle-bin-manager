package recycle.bin.manager;

import java.util.logging.Level;
import java.util.logging.Logger;

public class GUI_Options extends javax.swing.JFrame {
    private Interface iface = null;
    private Sys system;
    private java.util.Map<String, javax.swing.tree.DefaultMutableTreeNode> nodeMap = new java.util.HashMap<String, javax.swing.tree.DefaultMutableTreeNode>();
    private javax.swing.text.StyledDocument logView;
    private javax.swing.text.Style[] styles;
    public GUI_Options(Interface face) {
        iface = face; face = null;
        system = iface.getSystem();
        iface.getAppFunctions().isConfiguring = true;
        initComponents();
        try {
            initConfigs();
            logView = jTextPane_Log.getStyledDocument();
            setStyles();
            getLog();
            if(!iface.getAppFunctions().trialexpired){
                setIcons();
                setVisible(true);
            }
        } catch(Exception ex){
            java.util.logging.Logger.getLogger(GUI_Options.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //Window Listener
        addWindowListener(new java.awt.event.WindowAdapter() {
	    @Override
        public void windowClosing(java.awt.event.WindowEvent ex) {
                    try {
                        dispose();
                        setVisible(false);
                        setProperties();
                        iface.getAppFunctions().setConfig(iface.getAppFunctions().properties);
                        iface.getAppFunctions().isConfiguring = false;
                        iface.getAppFunctions().contents = iface.getAppFunctions().getContents();
                        iface.getGUI().getRecycleBinTable().updateUI();

                    } catch (Exception ex1) {
                        java.util.logging.Logger.getLogger(GUI_Options.class.getName()).log(java.util.logging.Level.SEVERE, null, ex1);
                    }
        }
        });
        java.awt.event.MouseListener jTree_MouseListener = new java.awt.event.MouseAdapter() {
            @Override
                public void mousePressed(java.awt.event.MouseEvent mc) {
                    int selRow = jTree_Extensions.getRowForLocation(mc.getX(), mc.getY());
                    javax.swing.tree.TreePath selPath = jTree_Extensions.getPathForLocation(mc.getX(), mc.getY());
                    if(selRow != -1) {
                        if(mc.getButton() == java.awt.event.MouseEvent.BUTTON1) {
                            if(mc.getClickCount() == 1 && selPath.getPathCount() > 1) {
                                String selection = selPath.toString().split(",")[selPath.getPathCount() - 1].replace("]", "").substring(1);
                                String parent = selPath.getParentPath().toString().split(",")[selPath.getPathCount() - 2].replace("]", "").substring(1);
                            } else if(mc.getClickCount() == 2) {
                                String selection = selPath.toString().split(",")[selPath.getPathCount() - 1].replace("]", "").substring(1);
                                String parent = selPath.getParentPath().toString().split(",")[selPath.getPathCount() - 2].replace("]", "").substring(1);
                            }
                        }
                    }
                }
        };
        jTree_Extensions.addMouseListener(jTree_MouseListener);

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel_General = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        jCheckBox_min2tray = new javax.swing.JCheckBox();
        jSlider_Frequency = new javax.swing.JSlider();
        jToggleButton_Enable = new javax.swing.JToggleButton();
        jComboBox_FrequencyUnit = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jToggleButton_Disable = new javax.swing.JToggleButton();
        jSpinner_Frequency = new javax.swing.JSpinner();
        jLabel_Icon = new javax.swing.JLabel();
        jCheckBox_ConfirmDialog = new javax.swing.JCheckBox();
        jCheckBox_BootStart = new javax.swing.JCheckBox();
        jCheckBox_StartMinimized = new javax.swing.JCheckBox();
        jButton_SetFont = new javax.swing.JButton();
        jButton_ExtensionsRestore = new javax.swing.JButton();
        jCheckBox_HideRecycleBin = new javax.swing.JCheckBox();
        jPanel_FileTypes = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel_Header = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jLabel_Retension = new javax.swing.JLabel();
        jLabel_Extension = new javax.swing.JLabel();
        jLabel_FileType = new javax.swing.JLabel();
        jTextField_Extension = new javax.swing.JTextField();
        jComboBox_FileType = new javax.swing.JComboBox();
        jSlider_Retension = new javax.swing.JSlider();
        jComboBox_RetensionUnit = new javax.swing.JComboBox();
        jCheckBox_Enabled = new javax.swing.JCheckBox();
        jLabel_RetensionDisplay = new javax.swing.JLabel();
        jCheckBox_SecureDelete = new javax.swing.JCheckBox();
        jButton_ExtensionNew = new javax.swing.JButton();
        jButton_ExtensionDelete = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTree_Extensions = new javax.swing.JTree();
        jButton_ExtensionSave = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jTextField_Search = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jPanel8 = new javax.swing.JPanel();
        jLabel_SecDelLevel = new javax.swing.JLabel();
        jComboBox_SecDelLevel = new javax.swing.JComboBox();
        jLabel_SecDelType = new javax.swing.JLabel();
        jComboBox_SecDelType = new javax.swing.JComboBox();
        jCheckBox_SecDelFileNameRnd = new javax.swing.JCheckBox();
        jLabel_SecDelPriority = new javax.swing.JLabel();
        jComboBox_SecDelPriority = new javax.swing.JComboBox();
        jCheckBox_SecDelINFO2 = new javax.swing.JCheckBox();
        jCheckBox_SecDelLimit = new javax.swing.JCheckBox();
        jSpinner_SecDelLimitSizeValue = new javax.swing.JSpinner();
        jLabel3 = new javax.swing.JLabel();
        jComboBox_SecDelLimitSizeUnit = new javax.swing.JComboBox();
        jSpinner_SecDelLimitPercent = new javax.swing.JSpinner();
        jLabel4 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jCheckBox_Scan4OldRBins = new javax.swing.JCheckBox();
        jCheckBox_DelEmptyFolder = new javax.swing.JCheckBox();
        jCheckBox_CustRendSleep = new javax.swing.JCheckBox();
        jCheckBox_McxRBM = new javax.swing.JCheckBox();
        jPanel9 = new javax.swing.JPanel();
        jLabel_Header1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jPanel10 = new javax.swing.JPanel();
        jLabel_Header2 = new javax.swing.JLabel();
        jLabel_LogLevel = new javax.swing.JLabel();
        jComboBox_LogLevel = new javax.swing.JComboBox();
        jLabel_LogRetension = new javax.swing.JLabel();
        jComboBox_LogRetensionUnit = new javax.swing.JComboBox();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTextPane_Log = new javax.swing.JTextPane();
        jSpinner_LogRetensionValue = new javax.swing.JSpinner();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);
        setResizable(false);

        jTabbedPane1.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        jTabbedPane1.setTabPlacement(javax.swing.JTabbedPane.BOTTOM);

        jPanel6.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel9.setFont(new java.awt.Font("Georgia", 1, 18));
        jLabel9.setText("General");

        org.jdesktop.layout.GroupLayout jPanel6Layout = new org.jdesktop.layout.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .add(183, 183, 183)
                .add(jLabel9)
                .addContainerGap(220, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel9)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel7.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jCheckBox_min2tray.setText("Minimize to Tray on Close");

        jSlider_Frequency.setMajorTickSpacing(15);
        jSlider_Frequency.setMaximum(60);
        jSlider_Frequency.setMinorTickSpacing(1);
        jSlider_Frequency.setPaintLabels(true);
        jSlider_Frequency.setPaintTicks(true);
        jSlider_Frequency.setValue(30);
        jSlider_Frequency.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider_FrequencyStateChanged(evt);
            }
        });

        buttonGroup1.add(jToggleButton_Enable);
        jToggleButton_Enable.setText("Manager On");
        jToggleButton_Enable.setPreferredSize(new java.awt.Dimension(93, 23));
        jToggleButton_Enable.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jToggleButton_EnableStateChanged(evt);
            }
        });
        jToggleButton_Enable.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton_EnableActionPerformed(evt);
            }
        });

        jComboBox_FrequencyUnit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Seconds", "Minutes", "Hours", "Days" }));
        jComboBox_FrequencyUnit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_FrequencyUnitActionPerformed(evt);
            }
        });

        jLabel2.setText("Recycle Bin Poll Interval");

        buttonGroup1.add(jToggleButton_Disable);
        jToggleButton_Disable.setSelected(true);
        jToggleButton_Disable.setText("Manager Off");

        jSpinner_Frequency.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSpinner_FrequencyStateChanged(evt);
            }
        });

        jLabel_Icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Recycled-Empty_Disabled-256x256.png"))); // NOI18N
        jLabel_Icon.setFocusable(false);

        jCheckBox_ConfirmDialog.setText("Confirmation Dialogs");
        jCheckBox_ConfirmDialog.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_ConfirmDialogActionPerformed(evt);
            }
        });

        jCheckBox_BootStart.setText("Run at Windows Start");
        jCheckBox_BootStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_BootStartActionPerformed(evt);
            }
        });

        jCheckBox_StartMinimized.setText("Start Minimized");

        jButton_SetFont.setText("Set Font");
        jButton_SetFont.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_SetFontActionPerformed(evt);
            }
        });

        jButton_ExtensionsRestore.setText("Restore Default Extensions");
        jButton_ExtensionsRestore.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_ExtensionsRestoreActionPerformed(evt);
            }
        });

        jCheckBox_HideRecycleBin.setText("Hide Desktop Recycle Bin");
        jCheckBox_HideRecycleBin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_HideRecycleBinActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel7Layout = new org.jdesktop.layout.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel7Layout.createSequentialGroup()
                        .add(10, 10, 10)
                        .add(jLabel2))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel7Layout.createSequentialGroup()
                        .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel7Layout.createSequentialGroup()
                                .add(jSlider_Frequency, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 356, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(jComboBox_FrequencyUnit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(jSpinner_Frequency, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)))
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel7Layout.createSequentialGroup()
                                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jButton_SetFont, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jButton_ExtensionsRestore, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBox_BootStart)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBox_StartMinimized)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBox_min2tray, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBox_ConfirmDialog)
                                    .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBox_HideRecycleBin))
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jLabel_Icon, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 247, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jPanel7Layout.createSequentialGroup()
                                .add(jToggleButton_Enable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 186, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(18, 18, 18)
                                .add(jToggleButton_Disable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 174, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(26, 26, 26)))
                        .add(22, 22, 22))))
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel7Layout.createSequentialGroup()
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel7Layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jCheckBox_BootStart)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBox_StartMinimized)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBox_min2tray)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBox_ConfirmDialog)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jCheckBox_HideRecycleBin)
                        .add(18, 18, 18)
                        .add(jButton_SetFont)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jButton_ExtensionsRestore))
                    .add(jLabel_Icon))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 12, Short.MAX_VALUE)
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jToggleButton_Disable)
                    .add(jToggleButton_Enable, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel2)
                .add(1, 1, 1)
                .add(jPanel7Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel7Layout.createSequentialGroup()
                        .add(jSpinner_Frequency, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 8, Short.MAX_VALUE)
                        .add(jComboBox_FrequencyUnit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jSlider_Frequency, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        org.jdesktop.layout.GroupLayout jPanel_GeneralLayout = new org.jdesktop.layout.GroupLayout(jPanel_General);
        jPanel_General.setLayout(jPanel_GeneralLayout);
        jPanel_GeneralLayout.setHorizontalGroup(
            jPanel_GeneralLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel_GeneralLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel_GeneralLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel6, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel_GeneralLayout.setVerticalGroup(
            jPanel_GeneralLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel_GeneralLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel6, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel7, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane1.addTab("     General     ", jPanel_General);

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_Header.setFont(new java.awt.Font("Georgia", 1, 18));
        jLabel_Header.setText("File Types");

        org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .add(172, 172, 172)
                .add(jLabel_Header)
                .addContainerGap(201, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel_Header)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jPanel5.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_Retension.setText("Retension:");

        jLabel_Extension.setText("Extension:");

        jLabel_FileType.setText("File Type:");

        jComboBox_FileType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Development", "System Test", "Ingetration Test", "Load Test", "Production" }));

        jSlider_Retension.setMajorTickSpacing(15);
        jSlider_Retension.setMaximum(60);
        jSlider_Retension.setMinorTickSpacing(1);
        jSlider_Retension.setPaintLabels(true);
        jSlider_Retension.setPaintTicks(true);
        jSlider_Retension.setSnapToTicks(true);
        jSlider_Retension.setValue(30);
        jSlider_Retension.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jSlider_RetensionStateChanged(evt);
            }
        });

        jComboBox_RetensionUnit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "seconds", "minutes", "hours", "days", "weeks", "months", "years" }));
        jComboBox_RetensionUnit.setSelectedIndex(3);
        jComboBox_RetensionUnit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox_RetensionUnitItemStateChanged(evt);
            }
        });

        jCheckBox_Enabled.setText("Enable Auto Delete");
        jCheckBox_Enabled.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/RecycledBinFlush16.png"))); // NOI18N
        jCheckBox_Enabled.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/RecycledBinManagerIcon.png"))); // NOI18N
        jCheckBox_Enabled.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_EnabledItemStateChanged(evt);
            }
        });
        jCheckBox_Enabled.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_EnabledActionPerformed(evt);
            }
        });

        jLabel_RetensionDisplay.setFont(new java.awt.Font("Tahoma", 1, 11));

        jCheckBox_SecureDelete.setText("Secure Delete Files");
        jCheckBox_SecureDelete.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/lock-off-16x16.png"))); // NOI18N
        jCheckBox_SecureDelete.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/lock-16x16.png"))); // NOI18N
        jCheckBox_SecureDelete.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jCheckBox_SecureDeleteItemStateChanged(evt);
            }
        });
        jCheckBox_SecureDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_SecureDeleteActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel5Layout = new org.jdesktop.layout.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jSlider_Retension, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 245, Short.MAX_VALUE)
                        .addContainerGap())
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel_Extension)
                            .add(jLabel_FileType)
                            .add(jLabel_Retension))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jComboBox_RetensionUnit, 0, 181, Short.MAX_VALUE)
                            .add(jTextField_Extension, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 181, Short.MAX_VALUE)
                            .add(jComboBox_FileType, 0, 181, Short.MAX_VALUE))
                        .add(10, 10, 10))
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(jCheckBox_Enabled)
                            .add(jCheckBox_SecureDelete))
                        .add(24, 24, 24)
                        .add(jLabel_RetensionDisplay, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 82, Short.MAX_VALUE)
                        .addContainerGap())))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_Extension)
                    .add(jTextField_Extension, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_FileType)
                    .add(jComboBox_FileType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_Retension)
                    .add(jComboBox_RetensionUnit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel5Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jSlider_Retension, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel_RetensionDisplay, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 22, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .add(jPanel5Layout.createSequentialGroup()
                        .add(jCheckBox_SecureDelete)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jCheckBox_Enabled))))
        );

        jButton_ExtensionNew.setForeground(new java.awt.Color(0, 51, 153));
        jButton_ExtensionNew.setText("New");
        jButton_ExtensionNew.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_ExtensionNewActionPerformed(evt);
            }
        });

        jButton_ExtensionDelete.setForeground(new java.awt.Color(153, 0, 51));
        jButton_ExtensionDelete.setText("Delete");
        jButton_ExtensionDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_ExtensionDeleteActionPerformed(evt);
            }
        });

        jTree_Extensions.setFont(new java.awt.Font("Tahoma", 1, 11));
        jTree_Extensions.setLargeModel(true);
        jScrollPane1.setViewportView(jTree_Extensions);

        jButton_ExtensionSave.setForeground(new java.awt.Color(0, 102, 0));
        jButton_ExtensionSave.setText("Save");
        jButton_ExtensionSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_ExtensionSaveActionPerformed(evt);
            }
        });

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/find.png"))); // NOI18N
        jLabel1.setLabelFor(jTextField_Search);
        jLabel1.setText("Search");
        jLabel1.setToolTipText("Find Extension");

        jTextField_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextField_SearchActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel_FileTypesLayout = new org.jdesktop.layout.GroupLayout(jPanel_FileTypes);
        jPanel_FileTypes.setLayout(jPanel_FileTypesLayout);
        jPanel_FileTypesLayout.setHorizontalGroup(
            jPanel_FileTypesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel_FileTypesLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel_FileTypesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(jPanel3, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel_FileTypesLayout.createSequentialGroup()
                        .add(jScrollPane1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 189, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jPanel_FileTypesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jButton_ExtensionSave, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jButton_ExtensionDelete, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jButton_ExtensionNew, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 273, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel_FileTypesLayout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(jLabel1)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(jTextField_Search, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 166, Short.MAX_VALUE))
                            .add(jPanel5, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel_FileTypesLayout.setVerticalGroup(
            jPanel_FileTypesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel_FileTypesLayout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel_FileTypesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel_FileTypesLayout.createSequentialGroup()
                        .add(jPanel5, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton_ExtensionNew)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton_ExtensionDelete)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jButton_ExtensionSave)
                        .add(15, 15, 15)
                        .add(jPanel_FileTypesLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 48, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jTextField_Search, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                    .add(jScrollPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 386, Short.MAX_VALUE))
                .addContainerGap())
        );

        jTabbedPane1.addTab("File Types", jPanel_FileTypes);

        jPanel8.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Digital Shredding", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Georgia", 1, 12))); // NOI18N

        jLabel_SecDelLevel.setText("Overwrite Level");

        jComboBox_SecDelLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel_SecDelType.setText("Overwrite Style");

        jComboBox_SecDelType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jCheckBox_SecDelFileNameRnd.setText("Randomize Shredded Files Names (Added privacy from undelete Apps)");

        jLabel_SecDelPriority.setText("Overwrite Priority");

        jComboBox_SecDelPriority.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jCheckBox_SecDelINFO2.setText("Digitally Shred Recycle Bin INFO2 files (Windows Vista/7)");
        jCheckBox_SecDelINFO2.setToolTipText("INFO2 contains file information regarding the original deleted file.\n\nFile Name\nFile Path\nFile Delete TimeStamp\nFile Size");

        jCheckBox_SecDelLimit.setText("Limit Files Larger Than");

        jLabel3.setText("to Shred first");

        jComboBox_SecDelLimitSizeUnit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jLabel4.setText("%");

        org.jdesktop.layout.GroupLayout jPanel8Layout = new org.jdesktop.layout.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel8Layout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel_SecDelType)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel8Layout.createSequentialGroup()
                        .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel_SecDelLevel, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel_SecDelPriority))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(jComboBox_SecDelPriority, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jComboBox_SecDelType, 0, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .add(org.jdesktop.layout.GroupLayout.TRAILING, jComboBox_SecDelLevel, 0, 304, Short.MAX_VALUE))))
                .add(17, 17, 17))
            .add(jPanel8Layout.createSequentialGroup()
                .add(jCheckBox_SecDelINFO2)
                .addContainerGap())
            .add(jPanel8Layout.createSequentialGroup()
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel8Layout.createSequentialGroup()
                        .add(jCheckBox_SecDelLimit)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jSpinner_SecDelLimitSizeValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 52, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(1, 1, 1)
                        .add(jComboBox_SecDelLimitSizeUnit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 57, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 75, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jSpinner_SecDelLimitPercent, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 40, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jLabel4))
                    .add(org.jdesktop.layout.GroupLayout.LEADING, jCheckBox_SecDelFileNameRnd))
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel8Layout.createSequentialGroup()
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_SecDelLevel)
                    .add(jComboBox_SecDelLevel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_SecDelType)
                    .add(jComboBox_SecDelType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel_SecDelPriority)
                    .add(jComboBox_SecDelPriority, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 8, Short.MAX_VALUE)
                .add(jCheckBox_SecDelINFO2)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jCheckBox_SecDelFileNameRnd)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel8Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jCheckBox_SecDelLimit)
                    .add(jSpinner_SecDelLimitSizeValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel3)
                    .add(jComboBox_SecDelLimitSizeUnit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jSpinner_SecDelLimitPercent, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel4))
                .add(8, 8, 8))
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Special Handling", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Georgia", 1, 12))); // NOI18N

        jCheckBox_Scan4OldRBins.setText("Scan for the coexistence of recycle bins from prior operating systems");
        jCheckBox_Scan4OldRBins.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_Scan4OldRBinsActionPerformed(evt);
            }
        });

        jCheckBox_DelEmptyFolder.setText("Immediately Delete Empty Files and Folders");
        jCheckBox_DelEmptyFolder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_DelEmptyFolderActionPerformed(evt);
            }
        });

        jCheckBox_CustRendSleep.setText("Low GUI Refresh (low cpu usage, may act choppy)");

        jCheckBox_McxRBM.setText("Manage Media Center Extender Users Recycle Bins (requires Admin Privs)");
        jCheckBox_McxRBM.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox_McxRBMActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jCheckBox_CustRendSleep)
                    .add(jCheckBox_DelEmptyFolder)
                    .add(jCheckBox_Scan4OldRBins)
                    .add(jCheckBox_McxRBM))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .add(13, 13, 13)
                .add(jCheckBox_Scan4OldRBins)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jCheckBox_DelEmptyFolder)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jCheckBox_CustRendSleep)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jCheckBox_McxRBM)
                .addContainerGap(37, Short.MAX_VALUE))
        );

        jPanel9.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_Header1.setFont(new java.awt.Font("Georgia", 1, 18));
        jLabel_Header1.setText("Advanced Settings");

        org.jdesktop.layout.GroupLayout jPanel9Layout = new org.jdesktop.layout.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel9Layout.createSequentialGroup()
                .add(135, 135, 135)
                .add(jLabel_Header1)
                .addContainerGap(159, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel_Header1)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel8, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel9, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel9, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel8, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Advanced", jPanel2);

        jPanel10.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_Header2.setFont(new java.awt.Font("Georgia", 1, 18));
        jLabel_Header2.setText("Recycle Bin Log");

        org.jdesktop.layout.GroupLayout jPanel10Layout = new org.jdesktop.layout.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel10Layout.createSequentialGroup()
                .add(142, 142, 142)
                .add(jLabel_Header2)
                .addContainerGap(177, Short.MAX_VALUE))
        );
        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel_Header2)
                .addContainerGap(17, Short.MAX_VALUE))
        );

        jLabel_LogLevel.setFont(new java.awt.Font("Tahoma", 1, 13));
        jLabel_LogLevel.setText("Level");

        jComboBox_LogLevel.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox_LogLevel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox_LogLevelActionPerformed(evt);
            }
        });

        jLabel_LogRetension.setFont(new java.awt.Font("Tahoma", 1, 13));
        jLabel_LogRetension.setText("Retension");

        jComboBox_LogRetensionUnit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        jScrollPane2.setViewportView(jTextPane_Log);

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 469, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel10, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel1Layout.createSequentialGroup()
                        .add(jLabel_LogLevel)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jComboBox_LogLevel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 148, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(18, 18, 18)
                        .add(jLabel_LogRetension)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jSpinner_LogRetensionValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 41, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(jComboBox_LogRetensionUnit, 0, 146, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jScrollPane2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 360, Short.MAX_VALUE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jComboBox_LogRetensionUnit, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel_LogLevel)
                    .add(jComboBox_LogLevel, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel_LogRetension)
                    .add(jSpinner_LogRetensionValue, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(10, 10, 10))
        );

        jTabbedPane1.addTab("Logging", jPanel1);

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jTabbedPane1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 504, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton_ExtensionNewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ExtensionNewActionPerformed
        jTextField_Extension.setEnabled(true);
        jTextField_Extension.setEditable(true);
        jTextField_Extension.setText("");
        jCheckBox_Enabled.setSelected(true);
        javax.swing.tree.TreePath[] treepaths = jTree_Extensions.getSelectionPaths();
        if(treepaths == null || treepaths.length > 1) {
            setAlwaysOnTop(false);
            Object[] options = {"Okay", "Cancel"};
            String text;
            text = " Please select a single File Type Group from the tree\n" +
                   " to add an extension, or select the tree root 'File Types'\n"+
                   " to add a new file type. ";
            iface.getGUI().warningDialog(text,"New Creation",options);
        }
}//GEN-LAST:event_jButton_ExtensionNewActionPerformed
    private void jButton_ExtensionDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ExtensionDeleteActionPerformed
        javax.swing.tree.TreePath[] treepaths = jTree_Extensions.getSelectionPaths();
        boolean confirmed = false;
        boolean trunkconfirm = false;
        for(int i=0;i < treepaths.length;i++) {
            Object[] paths = treepaths[i].getPath();
            if(paths.length < 2) continue;
                //Root or Nothing Selected
            if(paths.length == 2) {
                // Delete FileType Group
                boolean result = true;
                javax.swing.tree.DefaultMutableTreeNode node = nodeMap.get(paths[1].toString());
                if(Boolean.valueOf(iface.getAppFunctions().properties.getProperty("APP.CONFIRMDIALOG", "true")) && !trunkconfirm) {
                    setAlwaysOnTop(false);
                    Object[] options = {"Remove", "Cancel"};
                    String text = " Are you sure you would like to remove ";
                    if(treepaths.length == 1) {
                        text += paths[1].toString()+" with "+node.getChildCount()+" extensions? ";
                    } else {
                        text += "your file type selection? ";
                    }
                    result = iface.getGUI().confirmationDialog(
                                text,
                                "Removal Confirmation",
                                options
                                );
                }
                setAlwaysOnTop(true);
                if(result) {
                    trunkconfirm = true;
                    java.util.Enumeration e = node.children();
                    while(e.hasMoreElements()) {
                       javax.swing.tree.DefaultMutableTreeNode child = (javax.swing.tree.DefaultMutableTreeNode)e.nextElement();
                       child.removeFromParent();
                       nodeMap.remove(child.toString());
                       iface.getAppFunctions().extMap.remove(child.toString());
                    }
                    nodeMap.remove(node.toString());
                    String sql = "DELETE FROM APP.EXTENSIONS WHERE FILETYPE = '"+node.toString()+"'";
                    iface.getDB().Statement(sql);
                    node.removeFromParent();
                    iface.getAppFunctions().typeMap.remove(node.toString());
                    jTree_Extensions.updateUI();
                }
            } else {
                // Delete Extension    
                boolean result = true;
                javax.swing.tree.DefaultMutableTreeNode node = nodeMap.get(paths[2].toString());
                if(Boolean.valueOf(iface.getAppFunctions().properties.getProperty("APP.CONFIRMDIALOG", "true")) && !confirmed) {
                    setAlwaysOnTop(false);
                    Object[] options = {"Remove", "Cancel"};
                    String text = " Are you sure you would like to remove \n your selected extension";
                    if(treepaths.length > 1) text += "s ";
                    text += "?";
                    result = iface.getGUI().confirmationDialog(
                                text,
                                "Removal Confirmation",
                                options
                                );
                }
                setAlwaysOnTop(true);
                if(result) {
                    confirmed = true;
                    iface.getAppFunctions().extMap.remove(paths[2].toString());
                    node.removeFromParent();
                    nodeMap.remove(paths[2].toString());
                    jTree_Extensions.updateUI();
                    String sql = "DELETE FROM APP.EXTENSIONS WHERE NAME = '"+node.toString()+"'";
                }
            }
        }
}//GEN-LAST:event_jButton_ExtensionDeleteActionPerformed
    private void jButton_ExtensionSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ExtensionSaveActionPerformed
        javax.swing.tree.TreePath[] treepaths = jTree_Extensions.getSelectionPaths();
        if(treepaths == null || treepaths.length > 1) {
            setAlwaysOnTop(false);
            Object[] options = {"Okay", "Cancel"};
            String text;
            text = " Please select a single File Type Group from the tree\n" +
                   " to add an extension, or select the tree root 'File Types'\n"+
                   " to save a new file type. ";
            iface.getGUI().warningDialog(text,"Save",options);
            return;
        }
        javax.swing.tree.TreePath treepath = jTree_Extensions.getSelectionPath();
        if(jTextField_Extension.getText().length() > 0 && jTree_Extensions.getSelectionCount() == 1) {
            Object[] paths = treepath.getPath();
            javax.swing.tree.DefaultMutableTreeNode node;
            boolean replace = true;
            if(paths.length == 1 && !jTextField_Extension.getText().equals("Set Globally")) {
                // Create New File Type
                if(nodeMap.containsKey(jTextField_Extension.getText())) {
                        setAlwaysOnTop(false);
                        Object[] options = {"Okay", "Cancel"};
                        String text =
                               " The file type "+jTextField_Extension.getText()+" already exits\n";
                        iface.getGUI().warningDialog(text,"Creation Error",options);
                        setAlwaysOnTop(true);
                        return;
                }
                node = (javax.swing.tree.DefaultMutableTreeNode)nodeMap.get(paths[0].toString());
                createjTreeNode(jTree_Extensions, node, jTextField_Extension.getText());
                java.util.Map tmpMap = new java.util.HashMap<String, String>();
                tmpMap.put("Enabled", String.valueOf(jCheckBox_Enabled.isSelected()));
                tmpMap.put("RetensionUnit", jComboBox_RetensionUnit.getSelectedIndex());
                tmpMap.put("RetensionValue", jSlider_Retension.getValue());
                tmpMap.put("SecureDelete", String.valueOf(jCheckBox_SecureDelete));
                iface.getAppFunctions().typeMap.put(jTextField_Extension.getText(), tmpMap);
                jComboBox_FileType.addItem(jTextField_Extension.getText());
                if(!iface.getDB().doesValueExist("APP.FILETYPES", "NAME", jTextField_Extension.getText())){
                    String sql = "INSERT INTO APP.FILETYPES VALUES ("+
                                 "'"+jTextField_Extension.getText()+"',"+
                                 "'"+String.valueOf(jCheckBox_Enabled.isSelected())+"',"+
                                 jComboBox_RetensionUnit.getSelectedIndex()+","+
                                 jSlider_Retension.getValue()+","+
                                 "'"+String.valueOf(jCheckBox_SecureDelete.isSelected())+"'"+
                                 ")";
                    iface.getDB().Statement(sql);
                }
            } else if(paths.length > 1 && replace){
                // Create New Extension
                if(iface.getAppFunctions().extMap.containsKey(jTextField_Extension.getText())) {
                        setAlwaysOnTop(false);
                        Object[] options = {"Replace", "Cancel"};
                        String text = 
                               " The file extension "+jTextField_Extension.getText()+" already exits\n" +
                               " and is associated with "+iface.getAppFunctions().extMap.get(jTextField_Extension.getText())+" File Type.\n"+
                               " Would you like to replace it with this one? ";
                        replace = iface.getGUI().confirmationDialog(text,"Save Extension",options);
                        if(replace) {
                            iface.getAppFunctions().extMap.remove(jTextField_Extension.getText());
                            nodeMap.remove(jTextField_Extension.getText());
                        }
                        setAlwaysOnTop(true);
                }
                if(replace){
                    node = (javax.swing.tree.DefaultMutableTreeNode)nodeMap.get(paths[1].toString());
                    createjTreeNode(jTree_Extensions, node, jTextField_Extension.getText());
                    iface.getAppFunctions().extMap.put(jTextField_Extension.getText(), paths[1].toString());
                    String sql;
                    String extName = paths[1].toString();
                    String fileType = jTextField_Extension.getText();
                    if(iface.getDB().doesValueExist("APP.EXTENSIONS", "NAME", extName)){
                        sql = "UPDATE APP.EXTENSIONS SET FILETYPE = '"+fileType+"' WHERE NAME = '"+extName+"'";
                    } else {
                        sql = "INSERT INTO APP.EXTENSIONS VALUES ('"+extName+"', '"+fileType+"')";
                    }
                }
            }
            jTree_Extensions.updateUI();
            try {
                setProperties();
            } catch (Exception ex) {
                Logger.getLogger(GUI_Options.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
}//GEN-LAST:event_jButton_ExtensionSaveActionPerformed
    private void jTextField_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextField_SearchActionPerformed
        String searchString = jTextField_Search.getText().toString().toLowerCase();
        boolean found = false;
        if(nodeMap.containsKey(searchString)){
            javax.swing.tree.DefaultMutableTreeNode node = (javax.swing.tree.DefaultMutableTreeNode)nodeMap.get(searchString);
            javax.swing.tree.DefaultMutableTreeNode parent = (javax.swing.tree.DefaultMutableTreeNode)node.getParent();
            javax.swing.tree.TreePath parPath = jTree_Extensions.getNextMatch(parent.toString(), 0, javax.swing.text.Position.Bias.Forward);
            jTree_Extensions.expandPath(parPath);
            int r = 0;
            int[] rows = jTree_Extensions.getSelectionRows();
            if(rows != null) r = rows[0];
            javax.swing.tree.TreePath path = jTree_Extensions.getNextMatch(searchString, r, javax.swing.text.Position.Bias.Forward);
            if(path != null) {
                found = true;
                jTree_Extensions.setSelectionPath(path);
                java.awt.Rectangle rect = jTree_Extensions.getPathBounds(path);
                jScrollPane1.getViewport().scrollRectToVisible(rect);
            }
        }
        if(!found) {
            java.awt.Component comp = (java.awt.Component)jTextField_Search;
            java.awt.Color color = java.awt.Color.RED;
            setBGcolor(comp, color);
        }
}//GEN-LAST:event_jTextField_SearchActionPerformed
    private void jSlider_FrequencyPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jSlider_FrequencyPropertyChange
        //Delete
    }//GEN-LAST:event_jSlider_FrequencyPropertyChange
    private void jToggleButton_EnableActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton_EnableActionPerformed
        if(iface.getAppFunctions().trialexpired){
                About about = new About(iface);
                jToggleButton_Disable.setSelected(true);
                iface.getAppFunctions().manageFlag = false;
                setIcons();
        }
    }//GEN-LAST:event_jToggleButton_EnableActionPerformed
    private void jComboBox_FrequencyUnitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_FrequencyUnitActionPerformed
        if(jComboBox_FrequencyUnit.getItemCount() == 7 && jSlider_Frequency.getValue() == 0) {
            setSlider(jSlider_Frequency,jComboBox_FrequencyUnit);
        }
    }//GEN-LAST:event_jComboBox_FrequencyUnitActionPerformed
    private void jSlider_FrequencyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider_FrequencyStateChanged
        jSpinner_Frequency.setValue(jSlider_Frequency.getValue());
        iface.getAppFunctions().setThreadTimerValue(jComboBox_FrequencyUnit.getSelectedIndex(), jSlider_Frequency.getValue());
        if(jSlider_Frequency.getValue() < 1) {
            jToggleButton_Disable.setSelected(true);
            iface.getAppFunctions().manageFlag = jToggleButton_Enable.isSelected();
            setIcons();
            iface.getAppFunctions().properties.put("APP.ENABLED", "false");
        }
        setIcons();
    }//GEN-LAST:event_jSlider_FrequencyStateChanged
    private void jSpinner_FrequencyStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSpinner_FrequencyStateChanged
        int jspin = Integer.valueOf(jSpinner_Frequency.getValue().toString());
        if(jspin > jSlider_Frequency.getMaximum()) {
            jSlider_Frequency.setMaximum(jspin);
        }
        jSlider_Frequency.setValue(jspin);
    }//GEN-LAST:event_jSpinner_FrequencyStateChanged
    private void jToggleButton_EnableStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jToggleButton_EnableStateChanged
        if(iface.getAppFunctions().trialexpired){
            if(jToggleButton_Enable.isSelected()){
                jToggleButton_Disable.setSelected(true);
                setIcons();
                iface.getAppFunctions().manageFlag = false;
                iface.getGUI().set_jCheckBoxMenuItem_Enabled();
                iface.getAppFunctions().properties.put("APP.ENABLED", "false");
            }
            setAlwaysOnTop(false);
        } else {
            iface.getAppFunctions().manageFlag = jToggleButton_Enable.isSelected();
            iface.getGUI().set_jCheckBoxMenuItem_Enabled();
            iface.getAppFunctions().properties.put("APP.ENABLED", jToggleButton_Enable.isSelected());
        }
        setIcons();
    }//GEN-LAST:event_jToggleButton_EnableStateChanged
    private void jSlider_RetensionStateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jSlider_RetensionStateChanged
        if(evt.getSource().toString().contains("invalid")||!isVisible()) return;
        set_jLabel_RetensionDisplay(true);
        if(iface.getAppFunctions().typeMap.containsKey(jComboBox_FileType.getSelectedItem().toString())) {
            java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(jComboBox_FileType.getSelectedItem().toString());
            if(dMap.containsKey("RetensionUnit")) {
                jComboBox_RetensionUnit.setSelectedIndex(Integer.valueOf(dMap.get("RetensionUnit").toString()));
            }
            if(dMap.containsKey("Enabled")) {
                jCheckBox_Enabled.setSelected(Boolean.valueOf(dMap.get("Enabled").toString()));
            }
            if(dMap.containsKey("SecureDelete")) {
                jCheckBox_SecureDelete.setSelected(Boolean.valueOf(dMap.get("SecureDelete").toString()));
            }
            dMap.put("RetensionValue", String.valueOf(jSlider_Retension.getValue()));
            iface.getAppFunctions().typeMap.put(jComboBox_FileType.getSelectedItem().toString(), dMap);
        } else if(jTree_Extensions.getSelectionCount() == 1 && jTree_Extensions.getSelectionRows()[0] == 0) {
            java.util.Iterator iterator = iface.getAppFunctions().typeMap.keySet().iterator();
            while(iterator.hasNext()) {
                String key = iterator.next().toString();
                java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(key);
                dMap.put("RetensionValue", String.valueOf(jSlider_Retension.getValue()));
                iface.getAppFunctions().typeMap.put(key, dMap);
            }
        }
    }//GEN-LAST:event_jSlider_RetensionStateChanged
    private void jComboBox_RetensionUnitItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox_RetensionUnitItemStateChanged
        if(evt.getSource().toString().contains("invalid")||!isVisible()) return;
        setSlider(jSlider_Retension, jComboBox_RetensionUnit);
        if(iface.getAppFunctions().typeMap.containsKey(jComboBox_FileType.getSelectedItem().toString())) {
            java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(jComboBox_FileType.getSelectedItem().toString());
            jSlider_Retension.setValue(Integer.valueOf(dMap.get("RetensionValue").toString()));
            jCheckBox_Enabled.setSelected(Boolean.valueOf(dMap.get("Enabled").toString()));
            jCheckBox_SecureDelete.setSelected(Boolean.valueOf(dMap.get("SecureDelete").toString()));
            dMap.put("RetensionUnit", String.valueOf(jComboBox_RetensionUnit.getSelectedIndex()));
            iface.getAppFunctions().typeMap.put(jComboBox_FileType.getSelectedItem().toString(), dMap);
        } else if(jTree_Extensions.getSelectionCount() == 1 && jTree_Extensions.getSelectionRows()[0] == 0) {
            java.util.Iterator iterator = iface.getAppFunctions().typeMap.keySet().iterator();
            while(iterator.hasNext()) {
                String key = iterator.next().toString();
                java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(key);
                dMap.put("RetensionUnit", String.valueOf(jComboBox_RetensionUnit.getSelectedIndex()));
                iface.getAppFunctions().typeMap.put(key, dMap);
            }
        }
    }//GEN-LAST:event_jComboBox_RetensionUnitItemStateChanged
    private void jCheckBox_EnabledItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_EnabledItemStateChanged
        if(evt.getSource().toString().contains("invalid") || iface.getAppFunctions().trialexpired) return;
        boolean isEnabled = jCheckBox_Enabled.isSelected();
        jSlider_Retension.setEnabled(isEnabled);
        jComboBox_RetensionUnit.setEnabled(isEnabled);
        jCheckBox_SecureDelete.setEnabled(isEnabled);
        if(iface.getAppFunctions().typeMap.containsKey(jComboBox_FileType.getSelectedItem().toString())) {
            java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(jComboBox_FileType.getSelectedItem().toString());
            if(dMap.containsKey("RetensionUnit")) jComboBox_RetensionUnit.setSelectedIndex(Integer.valueOf(dMap.get("RetensionUnit").toString()));
            if(dMap.containsKey("RetensionValue")) jSlider_Retension.setValue(Integer.valueOf(dMap.get("RetensionValue").toString()));
            if(dMap.containsKey("SecureDelete")) jCheckBox_SecureDelete.setSelected(Boolean.valueOf(dMap.get("SecureDelete").toString()));
            dMap.put("Enabled", String.valueOf(isEnabled));
            iface.getAppFunctions().typeMap.put(jComboBox_FileType.getSelectedItem().toString(), dMap);
        } else if(jTree_Extensions.getSelectionCount() == 1 && jTree_Extensions.getSelectionRows()[0] == 0) {
            java.util.Iterator iterator = iface.getAppFunctions().typeMap.keySet().iterator();
            while(iterator.hasNext()) {
                String key = iterator.next().toString();
                java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(key);
                dMap.put("Enabled", String.valueOf(isEnabled));
                iface.getAppFunctions().typeMap.put(key, dMap);
            }
        }
        set_jLabel_RetensionDisplay(isEnabled);
    }//GEN-LAST:event_jCheckBox_EnabledItemStateChanged
    private void jCheckBox_ConfirmDialogActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_ConfirmDialogActionPerformed
    // Delete Method
    }//GEN-LAST:event_jCheckBox_ConfirmDialogActionPerformed
    private void jCheckBox_EnabledActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_EnabledActionPerformed
            jTree_Extensions.updateUI();
    }//GEN-LAST:event_jCheckBox_EnabledActionPerformed
    private void jCheckBox_BootStartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_BootStartActionPerformed
        if(jCheckBox_BootStart.isSelected()) {
            String path = RegTools.getRegVal_HKCU(iface.getAppFunctions().keyPath, "InstallPath")+"\\RecycleBinManager.exe";
            RegTools.setRegKey_HKCU("Software\\Microsoft\\Windows\\CurrentVersion\\Run", "Recycle Bin Manager", path);
        } else {
            RegTools.delRegValue_HKCU("Software\\Microsoft\\Windows\\CurrentVersion\\Run", "Recycle Bin Manager");
        }
    }//GEN-LAST:event_jCheckBox_BootStartActionPerformed
    private void jButton_SetFontActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_SetFontActionPerformed
        JFontChooser fontChooser = new JFontChooser();
        setAlwaysOnTop(false);
        String fontSet = iface.getAppFunctions().properties.getProperty("GUI.FONT", "Georgia,1,11");
        String[] fontAttrs = fontSet.split(",");
        String fontName = fontAttrs[0].toString();
        int fontStyle = Integer.valueOf(fontAttrs[1].toString().trim());
        int fontSize = Integer.valueOf(fontAttrs[2].toString().trim());
        java.awt.Font curFont = new java.awt.Font(fontName, fontStyle, fontSize);
        fontChooser.setSelectedFont(curFont);
        fontChooser.setSelectedFontSize(fontSize);
        fontChooser.setSelectedFontStyle(fontStyle);
        int result = fontChooser.showDialog(iface.getGUI());
        if (result == JFontChooser.OK_OPTION)
        {
                java.awt.Font font = fontChooser.getSelectedFont();
                system.setFontMap(font);
                iface.getAppFunctions().properties.setProperty("GUI.FONT", font.getName()+","+font.getStyle()+","+font.getSize());
                iface.getGUI().createTableModel(iface.getAppFunctions().contents);
                iface.getGUI().setGUIcolumns();
                iface.getAppFunctions().refreshRecycleBin();
                iface.getGUI().updateTableUI();

        }
        setAlwaysOnTop(true);
    }//GEN-LAST:event_jButton_SetFontActionPerformed
    private void jCheckBox_SecureDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_SecureDeleteActionPerformed
    }//GEN-LAST:event_jCheckBox_SecureDeleteActionPerformed
    private void jCheckBox_SecureDeleteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jCheckBox_SecureDeleteItemStateChanged
        if(evt.getSource().toString().contains("invalid")) return;
        boolean secureDelete = jCheckBox_SecureDelete.isSelected();
        if(iface.getAppFunctions().typeMap.containsKey(jComboBox_FileType.getSelectedItem().toString())) {
            java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(jComboBox_FileType.getSelectedItem().toString());
            jComboBox_RetensionUnit.setSelectedIndex(Integer.valueOf(dMap.get("RetensionUnit").toString()));
            jSlider_Retension.setValue(Integer.valueOf(dMap.get("RetensionValue").toString()));
            jCheckBox_Enabled.setSelected(Boolean.valueOf(dMap.get("Enabled").toString()));
            dMap.put("SecureDelete", String.valueOf(secureDelete));
            iface.getAppFunctions().typeMap.put(jComboBox_FileType.getSelectedItem().toString(), dMap);
        } else if(jTree_Extensions.getSelectionCount() == 1 && jTree_Extensions.getSelectionRows()[0] == 0) {
            java.util.Iterator iterator = iface.getAppFunctions().typeMap.keySet().iterator();
            while(iterator.hasNext()) {
                String key = iterator.next().toString();
                java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(key);
                dMap.put("SecureDelete", String.valueOf(secureDelete));
                iface.getAppFunctions().typeMap.put(key, dMap);
            }
        }
    }//GEN-LAST:event_jCheckBox_SecureDeleteItemStateChanged

    private void jCheckBox_DelEmptyFolderActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_DelEmptyFolderActionPerformed
        if(jCheckBox_DelEmptyFolder.isSelected()){
            iface.getAppFunctions().properties.setProperty("APP.DELETEEMPTYFOLDERS", "true");
        } else {
            iface.getAppFunctions().properties.setProperty("APP.DELETEEMPTYFOLDERS", "false");
        }
}//GEN-LAST:event_jCheckBox_DelEmptyFolderActionPerformed

    private void jCheckBox_Scan4OldRBinsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_Scan4OldRBinsActionPerformed
        if(jCheckBox_Scan4OldRBins.isSelected()){
            iface.getAppFunctions().properties.setProperty("APP.SECUREDELETEFILENAMERND", "true");
        } else {
            iface.getAppFunctions().properties.setProperty("APP.SECUREDELETEFILENAMERND", "false");
        }
}//GEN-LAST:event_jCheckBox_Scan4OldRBinsActionPerformed

    private void jComboBox_LogLevelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox_LogLevelActionPerformed
        if(logView != null)getLog();
    }//GEN-LAST:event_jComboBox_LogLevelActionPerformed

    private void jButton_ExtensionsRestoreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ExtensionsRestoreActionPerformed
        iface.getAppFunctions().extMap = new Extensions(iface,true).extensions;
    }//GEN-LAST:event_jButton_ExtensionsRestoreActionPerformed

    private void jCheckBox_HideRecycleBinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_HideRecycleBinActionPerformed
        iface.getAppFunctions().setDTRecycleBin(!jCheckBox_HideRecycleBin.isSelected());
        setAlwaysOnTop(false);
        javax.swing.JOptionPane.showMessageDialog(iface.getGUI(), "Select  your desktop and refresh (F5).", "Notification", javax.swing.JOptionPane.PLAIN_MESSAGE);
        setAlwaysOnTop(true);
    }//GEN-LAST:event_jCheckBox_HideRecycleBinActionPerformed

    private void jCheckBox_McxRBMActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox_McxRBMActionPerformed
         iface.getAppFunctions().properties.setProperty("APP.MANAGEMXCRBM", String.valueOf(jCheckBox_McxRBM.isSelected()));
    }//GEN-LAST:event_jCheckBox_McxRBMActionPerformed

    private void set_jLabel_RetensionDisplay(boolean visible) {
        if(visible && jComboBox_RetensionUnit.getItemCount() > 0) {
            jLabel_RetensionDisplay.setVisible(true);
            String text = jComboBox_RetensionUnit.getSelectedItem().toString();
            if(jSlider_Retension.getValue() == 1) {
                text = text.substring(0, (text.length() - 1));
            }
            jLabel_RetensionDisplay.setText(jSlider_Retension.getValue()+" "+text);
            if(jCheckBox_Enabled.isSelected()) {
                jLabel_RetensionDisplay.setForeground(java.awt.Color.BLUE.darker());
            } else {
                jLabel_RetensionDisplay.setForeground(java.awt.Color.RED.darker());
            }
        } else {
            jLabel_RetensionDisplay.setVisible(false);
        }
    }
    private void setBGcolor(final java.awt.Component component, java.awt.Color color) {
        component.setBackground(color);	    
        Thread thread = new Thread("FadeBGThread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                java.awt.Color bg;
                int bg_r,bg_g,bg_b;
                while (!component.getBackground().equals(java.awt.Color.WHITE)) {
                    try {
                        bg = component.getBackground();
                        bg_r=(bg.getRed()*3)/100 +bg.getRed()+1; if(bg_r > 255) bg_r=255;
                        bg_g=(bg.getGreen()*3)/100 +bg.getGreen()+1; if(bg_g > 255) bg_g=255;
                        bg_b=(bg.getBlue()*3)/100 +bg.getBlue()+1; if(bg_b > 255) bg_b=255;                          
                        component.setBackground(new java.awt.Color(bg_r, bg_g, bg_b));
                        Thread.sleep(33);
                    } catch (InterruptedException ex) {
                    }
                }
            }
        };        
        thread.start();
    }
    private void setProperties() throws Exception {
        java.util.Properties props = iface.getAppFunctions().getProperties();
        int i = 0;
        //General
        props.setProperty("GUI.MIN2TRAY", String.valueOf(jCheckBox_min2tray.isSelected()));
        props.setProperty("APP.STARTMIN", String.valueOf(jCheckBox_StartMinimized.isSelected()));
        props.setProperty("APP.BOOTSTART", String.valueOf(jCheckBox_BootStart.isSelected()));
        props.setProperty("APP.CONFIRMDIALOG", String.valueOf(jCheckBox_ConfirmDialog.isSelected()));
        props.setProperty("APP.ENABLED", String.valueOf(jToggleButton_Enable.isSelected()));
        props.setProperty("GUI.FREQUENCYVALUE", String.valueOf(jSlider_Frequency.getValue()));
        props.setProperty("GUI.FREQUENCYUNITINDEX", String.valueOf(jComboBox_FrequencyUnit.getSelectedIndex()));
        //Advanced
        props.setProperty("APP.SECUREDELETEINFO2", String.valueOf(jCheckBox_SecDelINFO2.isSelected()));
        props.setProperty("APP.DELETEEMPTYFOLDERS", String.valueOf(jCheckBox_DelEmptyFolder.isSelected()));
        props.setProperty("APP.MANAGEMXCRBM", String.valueOf(jCheckBox_McxRBM.isSelected()));
        props.setProperty("APP.SECUREDELETEFILENAMERND", String.valueOf(jCheckBox_SecDelFileNameRnd.isSelected()));
        props.setProperty("APP.SCANOLDRECBINS", String.valueOf(jCheckBox_Scan4OldRBins.isSelected()));
        props.setProperty("APP.SECUREDELETELEVEL", String.valueOf(jComboBox_SecDelLevel.getSelectedIndex()));
        props.setProperty("APP.SECUREDELETETYPE", String.valueOf(jComboBox_SecDelType.getSelectedIndex()));
        props.setProperty("APP.SECUREDELETEPRIORITY", String.valueOf(jComboBox_SecDelPriority.getSelectedIndex()));
        props.setProperty("APP.SECUREDELETELIMIT", String.valueOf(jCheckBox_SecDelLimit.isSelected()));
        props.setProperty("APP.SECUREDELETELIMITSIZE", String.valueOf(jSpinner_SecDelLimitSizeValue.getValue()));
        props.setProperty("APP.SECUREDELETELIMITUNIT", String.valueOf(jComboBox_SecDelLimitSizeUnit.getSelectedIndex()));
        props.setProperty("APP.SECUREDELETELIMITPERCENT", String.valueOf(jSpinner_SecDelLimitPercent.getValue()));
        props.setProperty("GUI.CUSTRENDSLEEP", String.valueOf(jCheckBox_CustRendSleep.isSelected()));
        //Logging
        props.setProperty("APP.LOGLEVEL", String.valueOf(jComboBox_LogLevel.getSelectedIndex()));
        props.setProperty("APP.LOGLEVELUNITINDEX", String.valueOf(jComboBox_LogRetensionUnit.getSelectedIndex()));
        props.setProperty("APP.LOGLEVELRETENSION", String.valueOf(jSpinner_LogRetensionValue.getValue()));
        //Update Properties
        iface.getAppFunctions().setProperties(props);
        iface.getAppFunctions().isConfigured = true;
        iface.getGUI().getRecycleBinTable().updateUI();
        //Update FileType Settings
        java.util.Iterator iterator = iface.getAppFunctions().typeMap.keySet().iterator();
        while(iterator.hasNext()){
            String filetype = iterator.next().toString();
            java.util.Map tmpMap = (java.util.Map)iface.getAppFunctions().typeMap.get(filetype);
            String enabled = tmpMap.get("Enabled").toString();
            int runit = Integer.valueOf(tmpMap.get("RetensionUnit").toString());
            int rval = Integer.valueOf(tmpMap.get("RetensionValue").toString());
            String sdel = tmpMap.get("SecureDelete").toString();
            String sql = null;
            if(iface.getDB().doesValueExist("APP.FILETYPES", "NAME", filetype)){
                sql = "UPDATE APP.FILETYPES " +
                        "SET ENABLED = '"+enabled+"'," +
                           "RETENSIONUNIT = "+runit+","+
                           "RETENSIONVALUE = "+rval+","+
                           "SECUREDELETE = '"+sdel+"'"+
                        " WHERE NAME = '"+filetype+"'";
            } else {
                sql = "INSERT INTO APP.EXTENSIONS VALUES ('"+filetype+"', '"+enabled+"', "+runit+", "+rval+", '"+sdel+"')";
            }
            iface.getDB().Statement(sql);
        }
    }
    public void createjTreeRoot() {
        javax.swing.tree.DefaultMutableTreeNode rootNode = new javax.swing.tree.DefaultMutableTreeNode("File Types");
        nodeMap.put("File Types", rootNode);
        jTree_Extensions.setLargeModel(true);
        jTree_Extensions.setBackground((java.awt.Color) system.colorMap.get("menu"));
        javax.swing.tree.DefaultTreeModel treeModel = new javax.swing.tree.DefaultTreeModel(rootNode);
        jTree_Extensions.setModel(treeModel);
        jTree_Extensions.setFont((java.awt.Font) system.colorMap.get("normal"));
        CustomTreeCellRenderer customtreeRenderer = new CustomTreeCellRenderer();
        jTree_Extensions.setCellRenderer(customtreeRenderer);
        jComboBox_FileType.removeAllItems();
        java.util.Iterator iterator;
        // Add typeMap to Tree (Sorted and for Folder and Unknown)
        iterator = iface.getAppFunctions().typeMap.keySet().iterator();
        String[] nodes = new String[iface.getAppFunctions().typeMap.size()];
        for(int i=0;i < nodes.length;i++){
            nodes[i] = iterator.next().toString();
        }
        nodes = system.sortArray(nodes);
        for(int i=0;i < nodes.length;i++) {
            String filetype = nodes[i];
            if(!nodeMap.containsKey(filetype)) {
                createjTreeNode(jTree_Extensions, rootNode, filetype);
                jComboBox_FileType.addItem(filetype);
            }
        }
        iterator = iface.getAppFunctions().extMap.values().iterator();
        java.util.Iterator iterator_keys = iface.getAppFunctions().extMap.keySet().iterator();
        while(iterator.hasNext()) {
            String filetype = iterator.next().toString();
            String extension = iterator_keys.next().toString();
            if(!nodeMap.containsKey(filetype)) {
                createjTreeNode(jTree_Extensions, rootNode, filetype);
                jComboBox_FileType.addItem(filetype);
            }
            createjTreeNode(jTree_Extensions, nodeMap.get(filetype), extension);
        }
        jTree_Extensions.updateUI();
    }
    private void createjTreeNode(javax.swing.JTree tree,javax.swing.tree.DefaultMutableTreeNode node,String title) {
        javax.swing.tree.DefaultMutableTreeNode newnode = new javax.swing.tree.DefaultMutableTreeNode(title);
        nodeMap.put(title, newnode);
        node.add(newnode);
    }
    private void setIcons(){
        javax.swing.ImageIcon icon;
        java.awt.Image appIcon;
        if(iface.getAppFunctions().manageFlag) {
            icon = new javax.swing.ImageIcon(system.getResource("/resources/appIcon.png"));
            appIcon = new javax.swing.ImageIcon(system.getResource("/resources/Recycled-Empty_Enabled-16.png")).getImage();
        } else {
            icon = new javax.swing.ImageIcon(system.getResource("/resources/Recycled-Empty_Disabled-256x256.png"));
            appIcon = new javax.swing.ImageIcon(system.getResource("/resources/Recycled-Empty_Disabled-16.png")).getImage();
        }
        jLabel_Icon.setIcon(icon);
        setIconImage(appIcon);
        if(iface.getGUI() != null){
            iface.getGUI().setIcons();
        }
    }
    private void setSlider(javax.swing.JSlider slider,javax.swing.JComboBox combobox) {
        int min;
        int max;
        int majr;
        int minr;
        switch (combobox.getSelectedIndex()) {
            case 0: min=0; max=90; minr=1; majr=15; break;   //Seconds
            case 1: min=0; max=90; minr=1; majr=15; break;   //Minutes
            case 2: min=0; max=48; minr=1; majr=6; break;   //Hours
            case 3: min=0; max=56; minr=1; majr=7; break;   //Days
            case 4: min=0; max=24; minr=1; majr=4; break;   //Weeks
            case 5: min=0; max=18; minr=1; majr=3; break;   //Months
            case 6: min=0; max=10; minr=1; majr=5; break;   //Years
            default: min=0; max=60; minr=1; majr=15; break;
        }
        slider.setMinimum(min);
        slider.setMaximum(max);
        slider.setMajorTickSpacing(majr);
        slider.setMinorTickSpacing(minr);
        java.util.Dictionary labels = slider.createStandardLabels(slider.getMajorTickSpacing());
        slider.setLabelTable(labels);
        slider.updateUI();
    }
    private void initConfigs() throws Exception {
        // Options GUI Icon & Title
        setTitle("Recycle Bin Manager Options");
        // Options GUI Location
        int guiLocX = iface.getGUI().getLocation().x;
        int guiLocY = iface.getGUI().getLocation().y;
        int guiDimW = iface.getGUI().getSize().width;
        int guiDimH = iface.getGUI().getSize().height;
        int optDimW = getSize().width;
        int optDimH = getSize().height;
        int optLocX = (guiLocX + (guiDimW/2) - (optDimW/2));
        int optLocY = (guiLocY + (guiDimH/2) - (optDimH/2));
        setLocation(optLocX, optLocY);
        // Init ComboBox Values
        java.util.ArrayList retensionUnits = new java.util.ArrayList();
        retensionUnits.add("Seconds");
        retensionUnits.add("Minutes");
        retensionUnits.add("Hours");
        retensionUnits.add("Days");
        retensionUnits.add("Weeks");
        retensionUnits.add("Months");
        retensionUnits.add("Years");
        jComboBox_RetensionUnit.removeAllItems();
        jComboBox_FrequencyUnit.removeAllItems();
        for(int i=0;i < retensionUnits.size();i++) {
            jComboBox_RetensionUnit.addItem(retensionUnits.get(i));
            jComboBox_FrequencyUnit.addItem(retensionUnits.get(i));
        }
        java.util.Properties props = iface.getAppFunctions().getProperties();
        //General Tab
        jCheckBox_min2tray.setSelected(Boolean.valueOf(props.getProperty("GUI.MIN2TRAY", "true")));
        jCheckBox_StartMinimized.setSelected(Boolean.valueOf(props.getProperty("APP.STARTMIN", "false")));
        jCheckBox_BootStart.setSelected(Boolean.valueOf(props.getProperty("APP.BOOTSTART", "true")));
        jCheckBox_ConfirmDialog.setSelected(Boolean.valueOf(props.getProperty("APP.CONFIRMDIALOG", "false")));
        jSlider_Frequency.setValue(Integer.valueOf(props.getProperty("GUI.FREQUENCYVALUE", "30")));
        jCheckBox_CustRendSleep.setSelected(Boolean.valueOf(props.getProperty("GUI.CUSTRENDSLEEP", "true")));
        boolean jToggleButton_Enabled = Boolean.valueOf(props.getProperty("APP.ENABLED", "false"));
        if(jToggleButton_Enabled){
            jToggleButton_Enable.setSelected(true);
            iface.getAppFunctions().manageFlag = true;
        } else {
            jToggleButton_Disable.setSelected(true);
            iface.getAppFunctions().manageFlag = false;
        }
        jSpinner_Frequency.setValue(jSlider_Frequency.getValue());
        jComboBox_FrequencyUnit.setSelectedIndex(Integer.valueOf(props.getProperty("GUI.FREQUENCYUNITINDEX", "0")));
        jComboBox_RetensionUnit.setSelectedIndex(3);
        jSlider_Retension.setValue(30);
        jCheckBox_HideRecycleBin.setSelected(!iface.getAppFunctions().getDTRecycleBinState());
        createjTreeRoot();
        //Advanced Tab
        jCheckBox_SecDelINFO2.setSelected(Boolean.valueOf(props.getProperty("APP.SECUREDELETEINFO2", "true")));
        jCheckBox_DelEmptyFolder.setSelected(Boolean.valueOf(props.getProperty("APP.DELETEEMPTYFOLDERS", "true")));
        jCheckBox_McxRBM.setSelected(Boolean.valueOf(props.getProperty("APP.MANAGEMXCRBM", "false")));
        jCheckBox_SecDelFileNameRnd.setSelected(Boolean.valueOf(props.getProperty("APP.SECUREDELETEFILENAMERND", "true")));
        jCheckBox_Scan4OldRBins.setSelected(Boolean.valueOf(props.getProperty("APP.SCANOLDRECBINS", "true")));
        jComboBox_SecDelLevel.removeAllItems();
        jComboBox_SecDelLevel.addItem("Single Overwrite (> 99.0% File Data Destroyed)");
        jComboBox_SecDelLevel.addItem("Double Overwrite (> 99.5% File Data Destroyed)");
        jComboBox_SecDelLevel.addItem("Triple Overwrite (> 99.9% File Data Destroyed)");
        jComboBox_SecDelLevel.setSelectedIndex(Integer.valueOf(props.getProperty("APP.SECUREDELETELEVEL", "0")));
        jComboBox_SecDelType.removeAllItems();
        jComboBox_SecDelType.addItem("Random");
        jComboBox_SecDelType.addItem("Zeroized (All Zeros)");
        jComboBox_SecDelType.addItem("Initialized (All Ones)");
        jComboBox_SecDelType.addItem("Randomized (Random AlphaNumeric)");
        jComboBox_SecDelType.setSelectedIndex(Integer.valueOf(props.getProperty("APP.SECUREDELETETYPE", "0")));
        jComboBox_SecDelPriority.removeAllItems();
        jComboBox_SecDelPriority.addItem("Minimum (recommended performance)");
        jComboBox_SecDelPriority.addItem("Normal");
        jComboBox_SecDelPriority.addItem("Maximum");
        jComboBox_SecDelPriority.setSelectedIndex(Integer.valueOf(props.getProperty("APP.SECUREDELETEPRIORITY", "0")));
        jCheckBox_SecDelLimit.setSelected(Boolean.valueOf(props.getProperty("APP.SECUREDELETELIMIT", "false")));
        jSpinner_SecDelLimitSizeValue.setValue(Integer.valueOf(props.getProperty("APP.SECUREDELETELIMITSIZE", "10")));
        jComboBox_SecDelLimitSizeUnit.removeAllItems();
        jComboBox_SecDelLimitSizeUnit.addItem("KB");
        jComboBox_SecDelLimitSizeUnit.addItem("MB");
        jComboBox_SecDelLimitSizeUnit.addItem("GB");
        jComboBox_SecDelLimitSizeUnit.setSelectedIndex(Integer.valueOf(props.getProperty("APP.SECUREDELETELIMITUNIT", "1")));
        jSpinner_SecDelLimitPercent.setValue(Integer.valueOf(props.getProperty("APP.SECUREDELETELIMITPERCENT", "1")));
        //Logging Tab
        jComboBox_LogLevel.removeAllItems();
        jComboBox_LogLevel.addItem("--Disabled--");
        jComboBox_LogLevel.addItem("Errors ONLY");
        jComboBox_LogLevel.addItem("Deletions UnSecure ONLY");
        jComboBox_LogLevel.addItem("Deletions All");
        jComboBox_LogLevel.addItem("Verbose Debugging");
        jComboBox_LogLevel.setSelectedIndex(Integer.valueOf(props.getProperty("APP.LOGLEVEL", "0")));
        jComboBox_LogRetensionUnit.removeAllItems();
        jComboBox_LogRetensionUnit.addItem("Entries");
        jComboBox_LogRetensionUnit.addItem("Hours");
        jComboBox_LogRetensionUnit.addItem("Days");
        jComboBox_LogRetensionUnit.addItem("Weeks");
        jComboBox_LogRetensionUnit.addItem("Months");
        jComboBox_LogRetensionUnit.setSelectedIndex(Integer.valueOf(props.getProperty("APP.LOGLEVELUNITINDEX", "0")));
        jSpinner_LogRetensionValue.setValue(Integer.valueOf(props.getProperty("APP.LOGLEVELRETENSION", "25")));
        jTextPane_Log.setEditable(false);
    }    

    protected void setConfigureType(int tabIndex, String fileType) {
        jTabbedPane1.setSelectedIndex(tabIndex);
        for(int i=0;i < jTree_Extensions.getVisibleRowCount() - 1;i++) {
            javax.swing.tree.TreePath path = jTree_Extensions.getPathForRow(i);
            Object[] objPaths = path.getPath();
            if(objPaths.length > 1) {
                if(objPaths[1].toString().equals(fileType)){
                    jTree_Extensions.setSelectionRow(i);
                }
            }
        }
    }

   //Style Document Methods
    private void getLog(){
        try {
            clearLogView();
            int limit = 100;
            int logLevel = jComboBox_LogLevel.getSelectedIndex();
            java.util.ArrayList logList = iface.getDB().RBMlog.getLog(logLevel,limit);
            for(int i=0;i<logList.size();i++){
                java.util.Map logMap = (java.util.Map)logList.get(i);
                String entry = logMap.get("ENTRY").toString();
                String time = logMap.get("TIMESTAMP").toString().split("\\.")[0];
                int styleref = Integer.valueOf(logMap.get("STYLE").toString());
                addLogViewEntry(time,entry,styleref);
            }
        } catch (javax.swing.text.BadLocationException ex) {
            java.util.logging.Logger.getLogger(GUI_Options.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }

    private void addLogViewEntry(String timestamp, String text, int styleref) {
        try {
            logView.insertString(logView.getLength(), timestamp+"   ", styles[6]);
            logView.insertString(logView.getLength(), text+"\n", styles[styleref]);
        } catch (javax.swing.text.BadLocationException ex) {
            java.util.logging.Logger.getLogger(GUI_Options.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    private void clearLogView()  throws javax.swing.text.BadLocationException {
        if(logView != null){
            logView.remove(0,logView.getLength());
        }
    }
    private javax.swing.text.Style setStyle(javax.swing.text.Style style,String font,boolean italic,boolean bold,boolean underline,int size,java.awt.Color fg,java.awt.Color bg){
        javax.swing.text.StyleConstants.setItalic(style, italic);
        javax.swing.text.StyleConstants.setBold(style, bold);
        javax.swing.text.StyleConstants.setUnderline(style, underline);
        javax.swing.text.StyleConstants.setFontFamily(style, font);
        javax.swing.text.StyleConstants.setFontSize(style, size);
        javax.swing.text.StyleConstants.setBackground(style, bg);
        javax.swing.text.StyleConstants.setForeground(style, fg);
        return(style);
    }

    private javax.swing.text.Style[] setStyles() {
            /*
     * 0 = header
     * 1 = normal Black
     * 2 = normal Black Italic
     * 3 = normal Black Bold
     * 4 = normal Black ItalicBold
     * 5 = normal Blue
     * 6 = normal Blue Italic
     * 7 = normal Blue Bold
     * 8 = normal Blue ItalicBold
     * 9 = normal Red
     * 10= normal Red Italic
     * 11= normal Red Bold
     * 12= normal Red ItalicBold
     * 13= normal Green
     * 14= normal Green Italic
     * 15= normal Green Bold
     * 16= normal Green ItalicBold
     * 17= normal Orange
     * 18= normal Orange Italic
     * 19= normal Orange Bold
     * 20= normal Orange ItalicBold
     * 21= normal Black Highlight Bold
     * 22= normal Magenta Darkened
     * 23= normal Red Underlined
    */
        styles = new javax.swing.text.Style[24];
        java.awt.Color bg = new java.awt.Color(255, 255, 255);
        javax.swing.text.Style styleH1 = logView.addStyle("header", null);
        styles[0] = setStyle(styleH1,"Georgia",false,true,false,18,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleN = logView.addStyle("normal", null);
        styles[1] = setStyle(styleN,"Georgia",false,false,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNI = logView.addStyle("normalItalic", null);
        styles[2] = setStyle(styleNI,"Georgia",true,false,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNB = logView.addStyle("normalBold", null);
        styles[3] = setStyle(styleNB,"Georgia",false,true,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNBI = logView.addStyle("normalBoldItalic", null);
        styles[4] = setStyle(styleNBI,"Georgia",true,true,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNblue = logView.addStyle("normalBlue", null);
        styles[5] = setStyle(styleNblue,"Georgia",false,false,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNblueI = logView.addStyle("normalBlueItalic", null);
        styles[6] = setStyle(styleNblueI,"Georgia",true,false,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNblueB = logView.addStyle("normalBlueBold", null);
        styles[7] = setStyle(styleNblueB,"Georgia",false,true,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNblueBI = logView.addStyle("normalBlueBoldItalic", null);
        styles[8] = setStyle(styleNblueBI,"Georgia",true,true,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNred = logView.addStyle("normalRed", null);
        styles[9] = setStyle(styleNred,"Georgia",false,false,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNredI = logView.addStyle("normalRedItalic", null);
        styles[10] = setStyle(styleNredI,"Georgia",true,false,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNredB = logView.addStyle("normalRedBold", null);
        styles[11] = setStyle(styleNredB,"Georgia",false,true,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNredBI = logView.addStyle("normalRedBoldItalic", null);
        styles[12] = setStyle(styleNredBI,"Georgia",true,true,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNgreen = logView.addStyle("normalGreen", null);
        styles[13] = setStyle(styleNgreen,"Georgia",false,false,false,12,java.awt.Color.GREEN.darker(),bg);
        javax.swing.text.Style styleNgreenI = logView.addStyle("normalGreenItalic", null);
        styles[14] = setStyle(styleNgreenI,"Georgia",true,false,false,12,java.awt.Color.GREEN,bg);
        javax.swing.text.Style styleNgreenB = logView.addStyle("normalGreenBold", null);
        styles[15] = setStyle(styleNgreenB,"Georgia",false,true,false,12,java.awt.Color.GREEN,bg);
        javax.swing.text.Style styleNgreenBI = logView.addStyle("normalGreenBoldItalic", null);
        styles[16] = setStyle(styleNgreenBI,"Georgia",true,true,false,12,java.awt.Color.GREEN,bg);
        javax.swing.text.Style styleNorange = logView.addStyle("normalOrange", null);
        styles[17] = setStyle(styleNorange,"Georgia",false,false,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleNorangeI = logView.addStyle("normalOrangeItalic", null);
        styles[18] = setStyle(styleNorangeI,"Georgia",true,false,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleNorangeB = logView.addStyle("normalOrangeBold", null);
        styles[19] = setStyle(styleNorangeB,"Georgia",false,true,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleNorangeBI = logView.addStyle("normalOrangeBoldItalic", null);
        styles[20] = setStyle(styleNorangeBI,"Georgia",true,true,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleHB = logView.addStyle("highliteBold", null);
        styles[21] = setStyle(styleHB,"Georgia",false,true,false,12,java.awt.Color.BLACK,java.awt.Color.YELLOW.brighter());
        javax.swing.text.Style styleQT = logView.addStyle("quotedNormal", null);
        styles[22] = setStyle(styleQT,"Georgia",false,false,false,12,java.awt.Color.MAGENTA.darker().darker(),bg);
        javax.swing.text.Style styleRU = logView.addStyle("RedUnderLined", null);
        styles[23] = setStyle(styleRU,"Georgia",false,false,true,12,java.awt.Color.RED,bg);
        styles[23] = styleRU;
        return(styles);
    }

   // Internal Classes
    class CustomTreeCellRenderer extends javax.swing.tree.DefaultTreeCellRenderer {

        public CustomTreeCellRenderer() {
            super();
        }
        @Override
        public java.awt.Component getTreeCellRendererComponent(
                    javax.swing.JTree tree,
                    Object value,
                    boolean selection,
                    boolean expanded,
                    boolean leaf,
                    int row,
                    boolean hasFocus) {
            super.getTreeCellRendererComponent(tree, value, selection, expanded, leaf, row, hasFocus);
            javax.swing.tree.DefaultMutableTreeNode node = (javax.swing.tree.DefaultMutableTreeNode)value;
            boolean isroot = node.isRoot();
            boolean disabled = false;
            boolean emptytrunk = false;
            if(node.toString().equals("Folder")||node.toString().equals("Unknown")) {
                leaf = false;
            }
            if(!isroot) {
                if(node.getParent().toString().equals("File Types")) {
                    java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(node.toString());
                    if(dMap != null){
                        if(dMap.containsKey("Enabled")){
                            disabled = !Boolean.valueOf(dMap.get("Enabled").toString());
                        } else {
                            disabled = true;
                        }
                    } else {
                        String sql = "DELETE FROM APP.FILETYPES WHERE NAME = '"+node.toString()+"'";
                        iface.getDB().Statement(sql);
                        iface.getAppFunctions().typeMap.remove(node.toString());
                    }
                    if(node.children() == javax.swing.tree.DefaultMutableTreeNode.EMPTY_ENUMERATION) {
                        emptytrunk = true;
                        leaf = true;
                    }
                }
            }
            setIconAndToolTip(node.getUserObject(), tree, expanded, leaf, isroot, selection, emptytrunk, disabled);
            if(selection && tree.getSelectionCount() == 1) {
                if(node.isRoot()) {
                    jTextField_Extension.setText("Set Globally");
                    jTextField_Extension.setEditable(false);
                    jComboBox_FileType.setEnabled(false);
                    if(jComboBox_FileType.getItemCount() != (iface.getAppFunctions().typeMap.size() + 1)) {
                        jComboBox_FileType.addItem("All");
                    }
                    jComboBox_FileType.setSelectedItem("All");
                    jComboBox_FileType.setEditable(false);
                    jComboBox_RetensionUnit.setEnabled(true);
                    jSlider_Retension.setEnabled(true);
                    jCheckBox_Enabled.setEnabled(true);
                    jCheckBox_SecureDelete.setEnabled(true);
                    set_jLabel_RetensionDisplay(true);
                } else if(!leaf || emptytrunk){
                    jTextField_Extension.setText("");
                    jTextField_Extension.setEditable(false);
                    jComboBox_FileType.setEnabled(false);
                    jComboBox_FileType.setSelectedItem(node.toString());
                    java.util.Map dMap = (java.util.Map)iface.getAppFunctions().typeMap.get(node.toString());
                    int retensionUnitIndex = Integer.valueOf(dMap.get("RetensionUnit").toString());
                    int retensionValue = Integer.valueOf(dMap.get("RetensionValue").toString());
                    jComboBox_RetensionUnit.setSelectedIndex(retensionUnitIndex);
                    jComboBox_RetensionUnit.setEnabled(true);
                    jSlider_Retension.setValue(retensionValue);
                    jSlider_Retension.setEnabled(true);
                    boolean state = Boolean.valueOf(dMap.get("Enabled").toString());
                    jCheckBox_Enabled.setSelected(state);
                    jCheckBox_Enabled.setEnabled(true);
                    boolean secured = Boolean.valueOf(dMap.get("SecureDelete").toString());
                    jCheckBox_SecureDelete.setEnabled(true);
                    jCheckBox_SecureDelete.setSelected(secured);
                    set_jLabel_RetensionDisplay(true);
                } else if(leaf) {
                    jTextField_Extension.setText(node.toString());
                    jTextField_Extension.setEditable(false);
                    jComboBox_FileType.setSelectedItem(iface.getAppFunctions().extMap.get(node.toString()));
                    jComboBox_FileType.setEnabled(false);
                    jComboBox_RetensionUnit.setEnabled(false);
                    jSlider_Retension.setEnabled(false);
                    jCheckBox_Enabled.setSelected(false);
                    jCheckBox_Enabled.setEnabled(false);
                    jCheckBox_SecureDelete.setSelected(false);
                    jCheckBox_SecureDelete.setEnabled(false);
                    set_jLabel_RetensionDisplay(false);
                }
            }
            return(this);
        }
        private void setIconAndToolTip(Object obj, javax.swing.JTree tree, boolean expanded, boolean leaf, boolean isroot, boolean selected, boolean emptytrunk, boolean disabled) {
            tree.setBackground((java.awt.Color)system.colorMap.get("menu"));
            setForeground((java.awt.Color)system.colorMap.get("black"));
            setBackgroundNonSelectionColor((java.awt.Color)system.colorMap.get("menu"));
            setBackgroundSelectionColor((java.awt.Color)system.colorMap.get("header"));
            javax.swing.Icon icon = null;
            if(obj instanceof String) {
                String objStr = (String)(obj);
                if(isroot) {
                    icon = new javax.swing.ImageIcon(system.getResource("/resources/TreeRoot.png"));
                    setIcon(icon);
                } else {
                    if(obj instanceof String) {
                        if(!leaf && disabled && selected) {                            
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_disabled_open.gif"));
                        } else if (!leaf && disabled && expanded){
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_disabled_open.gif"));
                        } else if (disabled){
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_disabled_close.gif"));
                        } else if (!leaf && selected){
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_open.gif"));
                        } else if(!leaf && expanded){
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_open.gif"));
                        } else if(!leaf) {
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_close.gif"));
                        } else if(emptytrunk && selected) {
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_empty_open.gif"));
                        } else if(emptytrunk && expanded){
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_empty_open.gif"));
                        } else if(emptytrunk) {
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/folder_empty_close.gif"));
                        }
                        if(icon == null) {
                            icon = new javax.swing.ImageIcon(system.getResource("/resources/File.png"));
                        }
                    }
                    else if(obj instanceof java.io.File) {
                        icon = system.getSystemFileIcon((java.io.File)obj);
                    }
                    setIcon(icon);
                }
                java.awt.image.BufferedImage bufferedIcon = null;
                try {
                    bufferedIcon = new java.awt.image.BufferedImage(icon.getIconWidth(), icon.getIconHeight(), java.awt.image.BufferedImage.TYPE_BYTE_INDEXED);
                } catch (Exception ex) {

                }
                java.awt.Graphics g = bufferedIcon.getGraphics();
                iconPaint(g);
            }
        }
        @Override
        public java.awt.Color getBackgroundNonSelectionColor() {
            return super.getBackgroundNonSelectionColor();
        }
        @Override
        public java.awt.Color getBackgroundSelectionColor() {
            return super.getBackgroundSelectionColor();
        }
        @Override
        public synchronized java.awt.dnd.DropTarget getDropTarget() {
            return super.getDropTarget();
        }
        public int getNEXT() {
            return NEXT;
        }
        public int getPREVIOUS() {
            return PREVIOUS;
        }
        @Override
        public void setBackgroundNonSelectionColor(java.awt.Color newColor) {
            super.setBackgroundNonSelectionColor(newColor);
        }
        @Override
        public void setBackgroundSelectionColor(java.awt.Color newColor) {
            super.setBackgroundSelectionColor(newColor);
        }
        @Override
        public void setForeground(java.awt.Color fg) {
            super.setForeground(fg);
        }
        public void iconPaint( java.awt.Graphics g ) {
            g.fillRect( 0, 0, getWidth() - 1, getHeight() - 1 );
            g.setColor((java.awt.Color)system.colorMap.get("menu"));
            this.paint(g);
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jButton_ExtensionDelete;
    private javax.swing.JButton jButton_ExtensionNew;
    private javax.swing.JButton jButton_ExtensionSave;
    private javax.swing.JButton jButton_ExtensionsRestore;
    private javax.swing.JButton jButton_SetFont;
    private javax.swing.JCheckBox jCheckBox_BootStart;
    private javax.swing.JCheckBox jCheckBox_ConfirmDialog;
    private javax.swing.JCheckBox jCheckBox_CustRendSleep;
    private javax.swing.JCheckBox jCheckBox_DelEmptyFolder;
    private javax.swing.JCheckBox jCheckBox_Enabled;
    private javax.swing.JCheckBox jCheckBox_HideRecycleBin;
    private javax.swing.JCheckBox jCheckBox_McxRBM;
    private javax.swing.JCheckBox jCheckBox_Scan4OldRBins;
    private javax.swing.JCheckBox jCheckBox_SecDelFileNameRnd;
    private javax.swing.JCheckBox jCheckBox_SecDelINFO2;
    private javax.swing.JCheckBox jCheckBox_SecDelLimit;
    private javax.swing.JCheckBox jCheckBox_SecureDelete;
    private javax.swing.JCheckBox jCheckBox_StartMinimized;
    private javax.swing.JCheckBox jCheckBox_min2tray;
    private javax.swing.JComboBox jComboBox_FileType;
    private javax.swing.JComboBox jComboBox_FrequencyUnit;
    private javax.swing.JComboBox jComboBox_LogLevel;
    private javax.swing.JComboBox jComboBox_LogRetensionUnit;
    private javax.swing.JComboBox jComboBox_RetensionUnit;
    private javax.swing.JComboBox jComboBox_SecDelLevel;
    private javax.swing.JComboBox jComboBox_SecDelLimitSizeUnit;
    private javax.swing.JComboBox jComboBox_SecDelPriority;
    private javax.swing.JComboBox jComboBox_SecDelType;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JLabel jLabel_Extension;
    private javax.swing.JLabel jLabel_FileType;
    private javax.swing.JLabel jLabel_Header;
    private javax.swing.JLabel jLabel_Header1;
    private javax.swing.JLabel jLabel_Header2;
    private javax.swing.JLabel jLabel_Icon;
    private javax.swing.JLabel jLabel_LogLevel;
    private javax.swing.JLabel jLabel_LogRetension;
    private javax.swing.JLabel jLabel_Retension;
    private javax.swing.JLabel jLabel_RetensionDisplay;
    private javax.swing.JLabel jLabel_SecDelLevel;
    private javax.swing.JLabel jLabel_SecDelPriority;
    private javax.swing.JLabel jLabel_SecDelType;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JPanel jPanel_FileTypes;
    private javax.swing.JPanel jPanel_General;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSlider jSlider_Frequency;
    private javax.swing.JSlider jSlider_Retension;
    private javax.swing.JSpinner jSpinner_Frequency;
    private javax.swing.JSpinner jSpinner_LogRetensionValue;
    private javax.swing.JSpinner jSpinner_SecDelLimitPercent;
    private javax.swing.JSpinner jSpinner_SecDelLimitSizeValue;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTextField jTextField_Extension;
    private javax.swing.JTextField jTextField_Search;
    private javax.swing.JTextPane jTextPane_Log;
    private javax.swing.JToggleButton jToggleButton_Disable;
    private javax.swing.JToggleButton jToggleButton_Enable;
    private javax.swing.JTree jTree_Extensions;
    // End of variables declaration//GEN-END:variables

}
