package recycle.bin.manager;

public class RegTools {

  public static String bytesToString(byte[] bytes){
      String value = "";
      if(bytes != null) {
          for(int i=0;i < bytes.length;i++) {
              char c = (char)(bytes[i] & 0xFF);
              value += c;
          }
      }
      bytes=null;
      return(value);
  }

  public static int[] getRegKeyInfo_HKCU(String keyPath){
      return(RegUtil.RegQueryInfoKey(getRegHandle_HKCU(keyPath)));
  }
  public static int[] getRegKeyInfo_HKLM(String keyPath){
      return(RegUtil.RegQueryInfoKey(getRegHandle_HKLM(keyPath)));
  }
  public static String getRegVal_HKCU(String keyPath, String keyName){
      int handle = getRegHandle_HKCU(keyPath);
      byte[] values = RegUtil.RegQueryValueEx(handle, keyName);
      RegUtil.RegCloseKey(handle);
      String value = bytesToString(values).trim();
      return(value);
  }
  public static String getRegVal_HKLM(String keyPath, String keyName){
      int handle = getRegHandle_HKLM(keyPath);
      byte[] values = RegUtil.RegQueryValueEx(handle, keyName);
      RegUtil.RegCloseKey(handle);
      String value = bytesToString(values).trim();
      return(value);
  }
  public static String getSubKeyByKeyName_HKCU(String keyPath, int index, int maxLength){
      int key = getRegHandle_HKCU(keyPath);
      String subKeyName = bytesToString(RegUtil.RegEnumKeyEx(key, index, maxLength));
      return(subKeyName);
  }
  public static void setRegKey_HKCU(String keyPath, String keyName, String value) {
     int handle = openRegHandle_HKCU(keyPath);
     RegUtil.RegSetValueEx(handle, keyName, value);
     RegUtil.RegCloseKey(handle);
  }
  public static void setRegKey_HKLM(String keyPath, String keyName, String value) {
    int handle = openRegHandle_HKLM(keyPath);
    RegUtil.RegSetValueEx(handle, keyName, value);
    RegUtil.RegCloseKey(handle);
  }

  public static void createKey_HKCU(String keyName) {
     int handle = RegUtil.RegCreateKeyEx(RegUtil.HKEY_CURRENT_USER, keyName)[RegUtil.NATIVE_HANDLE];
     RegUtil.RegCloseKey(handle);
  }
  public static void createKey_HKLM(String keyName) {
     int handle = RegUtil.RegCreateKeyEx(RegUtil.HKEY_LOCAL_MACHINE, keyName)[RegUtil.NATIVE_HANDLE];
     RegUtil.RegCloseKey(handle);
  }

  public static void delRegKey_HKCU(String keyName) {
      RegUtil.RegDeleteKey(RegUtil.HKEY_CURRENT_USER, keyName);
  }
  public static void delRegKey_HKLM(String keyName) {
      RegUtil.RegDeleteKey(RegUtil.HKEY_LOCAL_MACHINE, keyName);
  }

  public static void delRegValue_HKCU(String keyPath, String valueName) {
      int handle = openRegHandle_HKCU(keyPath);
      RegUtil.RegDeleteValue(handle, valueName);
      RegUtil.RegCloseKey(handle);
  }
  public static void delRegValue_HKLM(String keyPath, String valueName) {
      int handle = openRegHandle_HKLM(keyPath);
      RegUtil.RegDeleteValue(handle, valueName);
      RegUtil.RegCloseKey(handle);
  }

  public static int getRegHandle_HKCU(String keyPath) {
      return(RegUtil.RegCreateKeyEx(RegUtil.HKEY_CURRENT_USER, keyPath)[RegUtil.NATIVE_HANDLE]);
  }
  public static int getRegHandle_HKLM(String keyPath) {
      return(RegUtil.RegCreateKeyEx(RegUtil.HKEY_LOCAL_MACHINE, keyPath)[RegUtil.NATIVE_HANDLE]);
  }

  public static int openRegHandle_HKCU(String keyPath) {
      return(RegUtil.RegOpenKey(RegUtil.HKEY_CURRENT_USER, keyPath ,RegUtil.KEY_ALL_ACCESS)[RegUtil.NATIVE_HANDLE]);
  }
  public static int openRegHandle_HKLM(String keyPath) {
      return(RegUtil.RegOpenKey(RegUtil.HKEY_LOCAL_MACHINE, keyPath ,RegUtil.KEY_ALL_ACCESS)[RegUtil.NATIVE_HANDLE]);
  }
  // Internal Classes
  public static class RegUtil {

    public static final int HKEY_CURRENT_USER = 0x80000001;
    public static final int HKEY_LOCAL_MACHINE = 0x80000002;
    public static final int ERROR_SUCCESS = 0;
    public static final int ERROR_FILE_NOT_FOUND = 2;
    public static final int ERROR_ACCESS_DENIED = 5;
    public static final int NATIVE_HANDLE = 0;
    public static final int ERROR_CODE = 1;
    public static final int SUBKEYS_NUMBER = 0;
    public static final int VALUES_NUMBER = 2;
    public static final int MAX_KEY_LENGTH = 3;
    public static final int MAX_VALUE_NAME_LENGTH = 4;
    public static final int DISPOSITION = 2;
    public static final int REG_CREATED_NEW_KEY = 1;
    public static final int REG_OPENED_EXISTING_KEY = 2;
    public static final int NULL_NATIVE_HANDLE = 0;

    public static final int DELETE = 0x10000;
    public static final int KEY_QUERY_VALUE = 1;
    public static final int KEY_SET_VALUE = 2;
    public static final int KEY_CREATE_SUB_KEY = 4;
    public static final int KEY_ENUMERATE_SUB_KEYS = 8;
    public static final int KEY_READ = 0x20019;
    public static final int KEY_WRITE = 0x20006;
    public static final int KEY_ALL_ACCESS = 0xf003f;

    private static final java.util.prefs.Preferences userRoot = java.util.prefs.Preferences.userRoot();
    private static final java.util.prefs.Preferences systemRoot = java.util.prefs.Preferences.systemRoot();
    private static Class userClass = userRoot.getClass();
    private static java.lang.reflect.Method windowsRegOpenKey = null;
    private static java.lang.reflect.Method windowsRegCloseKey = null;
    private static java.lang.reflect.Method windowsRegCreateKeyEx = null;
    private static java.lang.reflect.Method windowsRegDeleteKey = null;
    private static java.lang.reflect.Method windowsRegFlushKey = null;
    private static java.lang.reflect.Method windowsRegQueryValueEx = null;
    private static java.lang.reflect.Method windowsRegSetValueEx = null;
    private static java.lang.reflect.Method windowsRegDeleteValue = null;
    private static java.lang.reflect.Method windowsRegQueryInfoKey = null;
    private static java.lang.reflect.Method windowsRegEnumKeyEx = null;
    private static java.lang.reflect.Method windowsRegEnumValue = null;

    static  {
        try {
            windowsRegOpenKey = userClass.getDeclaredMethod("WindowsRegOpenKey", new Class[] { int.class, byte[].class, int.class });
            windowsRegOpenKey.setAccessible(true);
            windowsRegCloseKey = userClass.getDeclaredMethod("WindowsRegCloseKey", new Class[] { int.class });
            windowsRegCloseKey.setAccessible(true);
            windowsRegCreateKeyEx = userClass.getDeclaredMethod("WindowsRegCreateKeyEx", new Class[] { int.class, byte[].class });
            windowsRegCreateKeyEx.setAccessible(true);
            windowsRegDeleteKey = userClass.getDeclaredMethod("WindowsRegDeleteKey", new Class[] { int.class, byte[].class });
            windowsRegDeleteKey.setAccessible(true);
            windowsRegFlushKey = userClass.getDeclaredMethod("WindowsRegFlushKey", new Class[] { int.class });
            windowsRegFlushKey.setAccessible(true);
            windowsRegQueryValueEx = userClass.getDeclaredMethod("WindowsRegQueryValueEx", new Class[] { int.class, byte[].class });
            windowsRegQueryValueEx.setAccessible(true);
            windowsRegSetValueEx = userClass.getDeclaredMethod("WindowsRegSetValueEx", new Class[] { int.class, byte[].class, byte[].class });
            windowsRegSetValueEx.setAccessible(true);
            windowsRegDeleteValue = userClass.getDeclaredMethod("WindowsRegDeleteValue", new Class[] { int.class, byte[].class });
            windowsRegDeleteValue.setAccessible(true);
            windowsRegQueryInfoKey = userClass.getDeclaredMethod("WindowsRegQueryInfoKey", new Class[] { int.class });
            windowsRegQueryInfoKey.setAccessible(true);
            windowsRegEnumKeyEx = userClass.getDeclaredMethod("WindowsRegEnumKeyEx", new Class[] { int.class, int.class, int.class });
            windowsRegEnumKeyEx.setAccessible(true);
            windowsRegEnumValue = userClass.getDeclaredMethod("WindowsRegEnumValue", new Class[] { int.class, int.class, int.class });
            windowsRegEnumValue.setAccessible(true);
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    public static int[] RegOpenKey(int hKey, byte[] subKey, int securityMask)  {
        try {
            return (int[]) windowsRegOpenKey.invoke(systemRoot, new Object[] {
                    new Integer(hKey), subKey, new Integer(securityMask) });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return new int[0];
    }

    public static int[] RegOpenKey(int hKey, String subKey, int securityMask) {
        return RegOpenKey(hKey, stringToByteArray(subKey), securityMask);
    }

    /**
     * Java wrapper for Windows registry API RegCloseKey()
     *
     * @param hKey
     *            The Native Handle to the Key
     * @return Int
     */
    public static int RegCloseKey(int hKey) {
        try {
            return ((Integer) windowsRegCloseKey.invoke(systemRoot,
                    new Object[] { new Integer(hKey) })).intValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return ERROR_ACCESS_DENIED;
    }

    /**
     * Java wrapper for Windows registry API RegCreateKeyEx()
     *
     * @param hKey
     *            The Native Handle to the Key
     * @param subKey
     *            The Sub key as a byte array
     * @return Int[3] etval[NATIVE_HANDLE] will be the Native Handle of the
     *         registry entry, retval[ERROR_CODE] will be the error code
     *         ERROR_SUCCESS means success retval[DISPOSITION] will indicate
     *         whether key was created or existing key was opened
     */
    public static int[] RegCreateKeyEx(int hKey, byte[] subKey) {
        try {
            return (int[]) windowsRegCreateKeyEx.invoke(systemRoot,
                    new Object[] { new Integer(hKey), subKey });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return new int[0];
    }

    /**
     * Java wrapper for Windows registry API RegCreateKeyEx()
     *
     * @param hKey
     *            The Native Hanle to the Key
     * @param subKey
     *            The Sub key
     * @return Int[3] etval[NATIVE_HANDLE] will be the Native Handle of the
     *         registry entry, retval[ERROR_CODE] will be the error code
     *         ERROR_SUCCESS means success retval[DISPOSITION] will indicate
     *         whether key was created or existing key was opened
     */
    public static int[] RegCreateKeyEx(int hKey, String subKey) {
        return RegCreateKeyEx(hKey, stringToByteArray(subKey));
    }

    /**
     * Java wrapper for Windows registry API RegDeleteKey()
     *
     * @param hKey
     *            The Native Handle to a Key
     * @param subKey
     *            The sub key to be deleted as a byte array
     * @return The Error Code
     */
    public static int RegDeleteKey(int hKey, byte[] subKey) {
        try {
            return ((Integer) windowsRegDeleteKey.invoke(systemRoot,
                    new Object[] { new Integer(hKey), subKey })).intValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return ERROR_ACCESS_DENIED;
    }

    /**
     * Java wrapper for Windows registry API RegDeleteKey()
     *
     * @param hKey
     *            The Native Handle to a Key
     * @param subKey
     *            The sub key to be deleted
     * @return The Error Code
     */
    public static int RegDeleteKey(int hKey, String subKey) {
        return RegDeleteKey(hKey, stringToByteArray(subKey));
    }

    /**
     * Java wrapper for Windows registry API RegFlushKey()
     *
     * @param hKey
     *            The native handle
     * @return the error code
     */
    public static int RegFlushKey(int hKey) {
        try {
            return ((Integer) windowsRegFlushKey.invoke(systemRoot,
                    new Object[] { new Integer(hKey) })).intValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return ERROR_ACCESS_DENIED;
    }

    /**
     * Java wrapper for Windows registry API RegQueryValueEx()
     *
     * @param hKey
     *            The Native Handle
     * @param valueName
     *            The Name of value to be queried as a byte array
     * @return The value queried
     */
    public static byte[] RegQueryValueEx(int hKey, byte[] valueName) {
        try {
            return (byte[]) windowsRegQueryValueEx.invoke(systemRoot,
                    new Object[] { new Integer(hKey), valueName });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * Java wrapper for Windows registry API RegQueryValueEx()
     *
     * @param hKey
     *            The Native Handle
     * @param valueName
     *            The Name of value to be queried
     * @return The value queried
     */
    public static byte[] RegQueryValueEx(int hKey, String valueName) {
        return RegQueryValueEx(hKey, stringToByteArray(valueName));
    }

    /**
     * Java wrapper for Windows registry API RegSetValueEx()
     * Creates a value if it didnt exist or will overwrite existing value
     *
     * @param hKey
     *            the Native Handle to the key
     * @param valueName
     *            The name of the value as a byet array
     * @param value
     *            The new vaue to be set as a byte array
     * @return The error code
     */
    public static int RegSetValueEx(int hKey, byte[] valueName, byte[] value) {
        try {
            return ((Integer) windowsRegSetValueEx.invoke(systemRoot,
                    new Object[] { new Integer(hKey), valueName, value }))
                    .intValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return ERROR_ACCESS_DENIED;
    }

    /**
     * Java wrapper for Windows registry API RegSetValueEx()
     * Creates a value if it didnt exist or will overwrite existing value
     *
     * @param hKey
     *            the Native Handle to the key
     * @param valueName
     *            The name of the value as a byet array
     * @param value
     *            The new vaue to be set as a byte array
     * @return The error code
     */
    public static int RegSetValueEx(int hKey, String valueName, String value) {
        return RegSetValueEx(hKey, stringToByteArray(valueName), stringToByteArray(value));
    }

    /**
     * Java wrapper for Windows registry API RegDeleteValue()
     *
     * @param hKey
     *            The native Handle
     * @param valueName
     *            The sub key name as a byte array
     * @return The error code
     */
    public static int RegDeleteValue(int hKey, byte[] valueName) {
        try {
            return ((Integer) windowsRegDeleteValue.invoke(systemRoot,
                    new Object[] { new Integer(hKey), valueName })).intValue();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return ERROR_ACCESS_DENIED;
    }

    /**
     * Java wrapper for Windows registry API RegDeleteValue()
     *
     * @param hKey
     *            The native Handle
     * @param valueName
     *            The sub key name
     * @return The error code
     */
    public static int RegDeleteValue(int hKey, String valueName) {
        return RegDeleteValue(hKey, stringToByteArray(valueName));
    }

    /**
     * Java wrapper for Windows registry API RegQueryInfoKey()
     *
     * @param hKey
     *            the Native Handle
     * @return int[5] retval[SUBKEYS_NUMBER] will give the number of sub keys
     *         under the queried key. retval[ERROR_CODE] will give the error
     *         code. retval[VALUES_NUMBER] will give the number of values under
     *         the given key. retval[MAX_KEY_LENGTH] will give the length of the
     *         longes sub key. retval[MAX_VALUE_NAME_LENGTH] will give length of
     *         the longes value name.
     */
    public static int[] RegQueryInfoKey(int hKey) {
        try {
            return (int[]) windowsRegQueryInfoKey.invoke(systemRoot,
                    new Object[] { new Integer(hKey) });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return new int[0];
    }

    /**
     * Java wrapper for Windows registry API RegEnumKeyEx()
     * @param hKey The native Handle
     * @param subKeyIndex Index of sub key
     * @param maxKeyLength The length of max sub key
     * @return The name of the sub key
     */
    public static byte[] RegEnumKeyEx(int hKey, int subKeyIndex, int maxKeyLength) {
        try {
            return (byte[]) windowsRegEnumKeyEx.invoke(systemRoot,
                    new Object[] { new Integer(hKey), new Integer(subKeyIndex),
                            new Integer(maxKeyLength) });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * Java wrapper for Windows registry API RegEnumValue()
     * @param hKey The handle
     * @param valueIndex the index of the value in the Key
     * @param maxValueNameLength The max length of name
     * @return The value
     */
    public static byte[] RegEnumValue(int hKey, int valueIndex, int maxValueNameLength) {
        try {
            return (byte[]) windowsRegEnumValue.invoke(systemRoot,
                    new Object[] { new Integer(hKey), new Integer(valueIndex),
                            new Integer(maxValueNameLength) });
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (java.lang.reflect.InvocationTargetException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    /**
     * Returns this java string as a null-terminated byte array
     */
    public static byte[] stringToByteArray(String str) {
        byte[] result = new byte[str.length() + 1];
        for (int i = 0; i < str.length(); i++) {
            result[i] = (byte) str.charAt(i);
        }
        result[str.length()] = 0;
        return result;
    }
}

}