package recycle.bin.manager;

public class About extends javax.swing.JFrame {
    //Global Vars
    Interface iface;
    javax.swing.text.StyledDocument view;
    javax.swing.text.Style[] styles;
    java.awt.Color bg;
    Sys system;
    public About(Interface face) {
        bg = new java.awt.Color(240, 240, 240);
        styles = new javax.swing.text.Style[24];
        iface = face;
        face = null;
        system = iface.getSystem();
        initComponents();
        //Set ScreenSize
        int scrDimW = java.awt.Toolkit.getDefaultToolkit().getScreenSize().width;
        int scrDimH = java.awt.Toolkit.getDefaultToolkit().getScreenSize().height;
        int abtDimW = getSize().width;
        int abtDimH = getSize().height;
        int abtLocX = ((scrDimW/2) - (abtDimW/2));
        int abtLocY = ( (scrDimH/2) - (abtDimH/2));
        setLocation(abtLocX, abtLocY);
        setIconImage(new javax.swing.ImageIcon(system.getResource("/resources/Recycled-Empty_Enabled-16.png")).getImage());
        view = new javax.swing.text.DefaultStyledDocument();
        styles = setStyles();
        jTextPane1.setStyledDocument(view);
        setContent();
        jTextField_RegName.setText(iface.getAppFunctions().user);
        jTextField_RegHost.setText(system.getHostname());
        jTextField_Version.setText("Version "+iface.getAppFunctions().VERSION+" "+iface.getAppFunctions().getTrialTimeRemaining());
        if(iface.getAppFunctions().registered) {
            jButton_PayPalPurchase.setEnabled(false);
            jButton_ActivateKey.setEnabled(false);
        }
        setVisible(true);
        setAlwaysOnTop(true);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTextPane1 = new javax.swing.JTextPane();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel_RegName = new javax.swing.JLabel();
        jTextField_RegName = new javax.swing.JTextField();
        jLabel_RegHost = new javax.swing.JLabel();
        jLabel_Version = new javax.swing.JLabel();
        jTextField_RegHost = new javax.swing.JTextField();
        jTextField_Version = new javax.swing.JTextField();
        jButton_VistHome = new javax.swing.JButton();
        jButton_PayPalPurchase = new javax.swing.JButton();
        jButton_ActivateKey = new javax.swing.JButton();
        jButton_VistSupport = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("About");
        setAlwaysOnTop(true);
        setIconImage(new javax.swing.ImageIcon(system.getResource("/resources/Recycled-Empty_Enabled-16.png")).getImage());
        setName("frame"); // NOI18N
        setResizable(false);

        jTextPane1.setBackground(new java.awt.Color(240, 240, 240));
        jTextPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jTextPane1.setEditable(false);
        jScrollPane1.setViewportView(jTextPane1);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/Banner400x100.png"))); // NOI18N

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel_RegName.setText("Registered To:");

        jTextField_RegName.setBackground(new java.awt.Color(240, 240, 240));
        jTextField_RegName.setEditable(false);

        jLabel_RegHost.setText("Reg Host:");

        jLabel_Version.setText("Version:");

        jTextField_RegHost.setBackground(new java.awt.Color(240, 240, 240));
        jTextField_RegHost.setEditable(false);

        jTextField_Version.setBackground(new java.awt.Color(240, 240, 240));
        jTextField_Version.setEditable(false);

        jButton_VistHome.setText("Visit Recycle Bin Manager Homepage!");
        jButton_VistHome.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_VistHomeActionPerformed(evt);
            }
        });

        jButton_PayPalPurchase.setForeground(new java.awt.Color(0, 102, 0));
        jButton_PayPalPurchase.setText("Purchase via PayPal");
        jButton_PayPalPurchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_PayPalPurchaseActionPerformed(evt);
            }
        });

        jButton_ActivateKey.setForeground(new java.awt.Color(255, 51, 51));
        jButton_ActivateKey.setText("Enter Activation Key");
        jButton_ActivateKey.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_ActivateKeyActionPerformed(evt);
            }
        });

        jButton_VistSupport.setForeground(new java.awt.Color(51, 51, 255));
        jButton_VistSupport.setText("Give Feedback");
        jButton_VistSupport.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton_VistSupportActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel_Version, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                            .addComponent(jLabel_RegHost, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 85, Short.MAX_VALUE)
                            .addComponent(jLabel_RegName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jTextField_Version, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_RegHost, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jTextField_RegName, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(122, 122, 122)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton_PayPalPurchase, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                            .addComponent(jButton_ActivateKey, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(122, 122, 122)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButton_VistSupport, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE)
                            .addComponent(jButton_VistHome, javax.swing.GroupLayout.DEFAULT_SIZE, 254, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_RegName)
                    .addComponent(jTextField_RegName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel_RegHost, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextField_RegHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel_Version)
                    .addComponent(jTextField_Version, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton_VistHome)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_VistSupport)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_PayPalPurchase)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton_ActivateKey))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 416, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private void jButton_VistHomeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_VistHomeActionPerformed
        dispose();
        system.launchBrowser("http://www.satalinksoft.com/index.php?p=1_9_RBM");
    }//GEN-LAST:event_jButton_VistHomeActionPerformed
    private void jButton_PayPalPurchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_PayPalPurchaseActionPerformed
        dispose();
        system.launchBrowser("https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=9296812");
    }//GEN-LAST:event_jButton_PayPalPurchaseActionPerformed
    private void jButton_ActivateKeyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_ActivateKeyActionPerformed
        setAlwaysOnTop(false);
        iface.getAppFunctions().registration();
        dispose();
    }//GEN-LAST:event_jButton_ActivateKeyActionPerformed

    private void jButton_VistSupportActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton_VistSupportActionPerformed
        dispose();
        system.sendMail("mailto:support@satalinksoft.com?subject=RBM%20v"+iface.getAppFunctions().VERSION+"%20Feedback");
    }//GEN-LAST:event_jButton_VistSupportActionPerformed

    private void setHeader(String headerText){
        try {
            clearDoc();
            view.insertString(view.getLength(), "\n\n\t" + headerText + "\n\n", styles[0]);
        } catch (javax.swing.text.BadLocationException ex) {
            java.util.logging.Logger.getLogger(About.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    private void setText(String text, int styleref) {
        try {
            view.insertString(view.getLength(), text, styles[styleref]);
        } catch (javax.swing.text.BadLocationException ex) {
            java.util.logging.Logger.getLogger(About.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
    }
    private void clearDoc()  throws javax.swing.text.BadLocationException {
        view.remove(0, view.getLength());
    }
    private javax.swing.text.Style setStyle(javax.swing.text.Style style,String font,boolean italic,boolean bold,boolean underline,int size,java.awt.Color fg,java.awt.Color bg){
        javax.swing.text.StyleConstants.setItalic(style, italic);
        javax.swing.text.StyleConstants.setBold(style, bold);
        javax.swing.text.StyleConstants.setUnderline(style, underline);
        javax.swing.text.StyleConstants.setFontFamily(style, font);
        javax.swing.text.StyleConstants.setFontSize(style, size);
        javax.swing.text.StyleConstants.setBackground(style, bg);
        javax.swing.text.StyleConstants.setForeground(style, fg);
        return(style);
    }
    
    private javax.swing.text.Style[] setStyles() {
            /*
     * 0 = header
     * 1 = normal Black
     * 2 = normal Black Italic
     * 3 = normal Black Bold
     * 4 = normal Black ItalicBold
     * 5 = normal Blue
     * 6 = normal Blue Italic
     * 7 = normal Blue Bold
     * 8 = normal Blue ItalicBold
     * 9 = normal Red
     * 10= normal Red Italic
     * 11= normal Red Bold
     * 12= normal Red ItalicBold
     * 13= normal Green
     * 14= normal Green Italic
     * 15= normal Green Bold
     * 16= normal Green ItalicBold
     * 17= normal Orange
     * 18= normal Orange Italic
     * 19= normal Orange Bold
     * 20= normal Orange ItalicBold
     * 21= normal Black Highlight Bold
     * 22= normal Magenta Darkened
     * 23= normal Red Underlined
    */
        javax.swing.text.Style styleH1 = view.addStyle("header", null);
        styles[0] = setStyle(styleH1,"Georgia",false,true,false,18,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleN = view.addStyle("normal", null);
        styles[1] = setStyle(styleN,"Georgia",false,false,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNI = view.addStyle("normalItalic", null);
        styles[2] = setStyle(styleNI,"Georgia",true,false,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNB = view.addStyle("normalBold", null);
        styles[3] = setStyle(styleNB,"Georgia",false,true,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNBI = view.addStyle("normalBoldItalic", null);
        styles[4] = setStyle(styleNBI,"Georgia",true,true,false,12,java.awt.Color.BLACK,bg);
        javax.swing.text.Style styleNblue = view.addStyle("normalBlue", null);
        styles[5] = setStyle(styleNblue,"Georgia",false,false,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNblueI = view.addStyle("normalBlueItalic", null);
        styles[6] = setStyle(styleNblueI,"Georgia",true,false,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNblueB = view.addStyle("normalBlueBold", null);
        styles[7] = setStyle(styleNblueB,"Georgia",false,true,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNblueBI = view.addStyle("normalBlueBoldItalic", null);
        styles[8] = setStyle(styleNblueBI,"Georgia",true,true,false,12,java.awt.Color.BLUE,bg);
        javax.swing.text.Style styleNred = view.addStyle("normalRed", null);
        styles[9] = setStyle(styleNred,"Georgia",false,false,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNredI = view.addStyle("normalRedItalic", null);
        styles[10] = setStyle(styleNredI,"Georgia",true,false,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNredB = view.addStyle("normalRedBold", null);
        styles[11] = setStyle(styleNredB,"Georgia",false,true,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNredBI = view.addStyle("normalRedBoldItalic", null);
        styles[12] = setStyle(styleNredBI,"Georgia",true,true,false,12,java.awt.Color.RED,bg);
        javax.swing.text.Style styleNgreen = view.addStyle("normalGreen", null);
        styles[13] = setStyle(styleNgreen,"Georgia",false,false,false,12,java.awt.Color.GREEN.darker(),bg);
        javax.swing.text.Style styleNgreenI = view.addStyle("normalGreenItalic", null);
        styles[14] = setStyle(styleNgreenI,"Georgia",true,false,false,12,java.awt.Color.GREEN,bg);
        javax.swing.text.Style styleNgreenB = view.addStyle("normalGreenBold", null);
        styles[15] = setStyle(styleNgreenB,"Georgia",false,true,false,12,java.awt.Color.GREEN,bg);
        javax.swing.text.Style styleNgreenBI = view.addStyle("normalGreenBoldItalic", null);
        styles[16] = setStyle(styleNgreenBI,"Georgia",true,true,false,12,java.awt.Color.GREEN,bg);
        javax.swing.text.Style styleNorange = view.addStyle("normalOrange", null);
        styles[17] = setStyle(styleNorange,"Georgia",false,false,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleNorangeI = view.addStyle("normalOrangeItalic", null);
        styles[18] = setStyle(styleNorangeI,"Georgia",true,false,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleNorangeB = view.addStyle("normalOrangeBold", null);
        styles[19] = setStyle(styleNorangeB,"Georgia",false,true,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleNorangeBI = view.addStyle("normalOrangeBoldItalic", null);
        styles[20] = setStyle(styleNorangeBI,"Georgia",true,true,false,12,java.awt.Color.ORANGE,bg);
        javax.swing.text.Style styleHB = view.addStyle("highliteBold", null);
        styles[21] = setStyle(styleHB,"Georgia",false,true,false,12,java.awt.Color.BLACK,java.awt.Color.YELLOW.brighter());
        javax.swing.text.Style styleQT = view.addStyle("quotedNormal", null);
        styles[22] = setStyle(styleQT,"Georgia",false,false,false,12,java.awt.Color.MAGENTA.darker().darker(),bg);
        javax.swing.text.Style styleRU = view.addStyle("RedUnderLined", null);
        styles[23] = setStyle(styleRU,"Georgia",false,false,true,12,java.awt.Color.RED,bg);
        styles[23] = styleRU;
        return(styles);
    }
    private void setContent() {
        setText("\n    "+iface.getAppFunctions().PROGNAME+" was developed by ",1);
        setText("Satalink Soft LLC.\n",7);
        setText("                                   All rights reserved (c)2010.\n",1);
    }



    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton_ActivateKey;
    private javax.swing.JButton jButton_PayPalPurchase;
    private javax.swing.JButton jButton_VistHome;
    private javax.swing.JButton jButton_VistSupport;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel_RegHost;
    private javax.swing.JLabel jLabel_RegName;
    private javax.swing.JLabel jLabel_Version;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextField_RegHost;
    private javax.swing.JTextField jTextField_RegName;
    private javax.swing.JTextField jTextField_Version;
    private javax.swing.JTextPane jTextPane1;
    // End of variables declaration//GEN-END:variables

}
