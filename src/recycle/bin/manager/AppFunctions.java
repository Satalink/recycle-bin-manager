package recycle.bin.manager;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class AppFunctions {
    private Interface iface;
    private Sys system;
    public String user;
    public String PROGNAME = "Recycle Bin Manager";
    public String VERSION = "2.11";
    protected java.util.Properties properties;
    protected boolean registered = false;
    protected boolean trialexpired = false;
    protected boolean isConfiguring = true;
    protected boolean isConfigured = false;
    protected String sid;
    protected java.util.ArrayList contents = null;
    protected java.io.File[] roots = java.io.File.listRoots();
    protected java.util.Collection rbin;
    protected java.io.File[] rfiles;
    protected java.util.Map extMap;
    protected java.util.Map typeMap = new java.util.HashMap<String, java.util.Map>();
    protected String keyPath;
    public long appStartTime = 0;
    public long threadtimer = 300000;
    public long trial = 3;
    public boolean manageFlag = true;

    @SuppressWarnings("UseOfObsoleteCollectionType")
    public AppFunctions(Interface face) {
        iface = face; face = null;
        system = iface.getSystem();
        keyPath = "Software\\Satalink Soft\\"+PROGNAME;
        appStartTime = system.getEpoch();
        face = null;
        rbin = new java.util.Vector<java.io.File>();
        sid = iface.getSystem().getSID();
        getConfig();
        try {
            getLocations();
        } catch (javax.swing.text.BadLocationException ex) {
            Logger.getLogger(AppFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        isConfiguring = false;
        System.out.println("Functions OK.");
    }

    protected void exitApp(boolean atgui){
        boolean result = true;
        if(Boolean.valueOf(properties.getProperty("APP.CONFIRMDIALOG", "true").toString())) {
            Object[] options = {"Exit", "Cancel"};
            result = iface.getGUI().confirmationDialog(
                        "Are you sure you would like exit "+PROGNAME+"?  ",
                        "Exit Confirmation",
                        options
                        );
        }
        if(result) {
            if(iface.getGUI().isShowing()) {
                iface.getGUI().dispose();
            }
            if(!atgui){
                iface.getGUI().systemTray(false, "");
            }
            setConfig(properties);
            iface.getDB().DBclose();
            System.exit(0);
        }
    }

    protected void checkReg() {
        String usr = properties.getProperty("APP.USER", "Unregistered");
        String key = properties.getProperty("APP.KEY", "");
        if(!usr.equals("Unregistered")||trialexpired){
            registrationValidate(key,usr);
        }
        getTrialExpiration();
    }
    protected void DeleteThread(final String filepath, final boolean secureDelete){
        java.util.concurrent.Semaphore throttle = new java.util.concurrent.Semaphore(3,true);
        throttle.acquireUninterruptibly();
        final boolean secDelInfo2 = Boolean.valueOf(properties.getProperty("APP.SECUREDELETEINFO2", "true"));
        final int secLevel = Integer.valueOf(properties.getProperty("APP.SECUREDELETELEVEL", "2"));
        final int secType = Integer.valueOf(properties.getProperty("APP.SECUREDELETETYPE", "0"));
        final boolean secDelFileNameRnd = Boolean.valueOf(properties.getProperty("APP.SECUREDELETEFILENAMERND", "true"));
        Thread thread = new Thread("RecycleBin Delete Thread") {
            @Override
            @SuppressWarnings("static-access")
            public void run() {
                system.deleteFile(filepath, secureDelete, secLevel, secType, secDelInfo2, secDelFileNameRnd);
            }
        };
        int[] threadPriorities = {2,5,7};
        int threadPriority = threadPriorities[Integer.valueOf(properties.getProperty("APP.SECUREDELTEPRIORITY", "0"))];
        thread.setPriority(threadPriority);
        thread.setDaemon(true);
        thread.start();
        throttle.release();
    }
    public int getContentCount(){
        int fileCount = 0;
        java.util.Iterator iterator = rbin.iterator();
        while(iterator.hasNext()){
            java.io.File rFile = (java.io.File)iterator.next();
            if(rFile.exists()) fileCount += rFile.listFiles().length;
        }
        return(fileCount);
    }
    public java.util.ArrayList getContents() {
        java.util.ArrayList resultList = new java.util.ArrayList();
        java.util.Iterator iterator = rbin.iterator();
        java.io.File[] files = null;
        while(iterator.hasNext()){
            java.io.File rFile = (java.io.File)iterator.next();
            if(rFile.exists()){
                files = rFile.listFiles();
            }
            if(files != null) {
                if(system.getOS().contains("Vista")||system.getOS().endsWith("7")) {
                    resultList.addAll(getFileInfoVista(files));
                } else {
                    resultList.addAll(getFileInfoXP(files));
                }
            }
        }
        return(resultList);
    }
    public java.util.ArrayList getFileInfoXP(java.io.File[] files) {
        java.util.ArrayList fileInfoList = new java.util.ArrayList();
        java.io.File info;
        if(files != null){
            info = new java.io.File(files[0].getParentFile().getAbsolutePath()+java.io.File.separatorChar+"INFO2");
        } else {
            return(fileInfoList);
        }
        if(info.exists()){
            // Process INFO2
            java.util.ArrayList ifilecontents = system.getFileOpen(info);
                String filePath = "";
                int recByte = 0;
                byte[] dateBytes = new byte[8];
                byte[] recIDBytes = new byte[4];
            for(int c=0;c < ifilecontents.size();c++){
                byte[] bytes = (byte[])ifilecontents.get(c);
                /*
                 * Bytes 0 - 7   = File Header
                 * Bytes 8 - 11  = Number of Entries
                 * Bytes 12, 15  = Entry Record Size (20 03 hex = 800bytes)

                 *
                 * After first 16 bytes, the first record starts
                 * each record is 800 bytes long
                 *
                 * Record INFO:  Record Bytes are by Record position
                 * Record 1:  bytes 17-816  == 800
                 * Bytes 0 - 3  =  Record Header
                 * Bytes 4 - 263 =  FileNamePath ASCII
                 * Bytes 264 - 267 = Record Count
                 * Bytes 268 - 271 = Drive Identifier
                 * Bytes 272 - 279 = File Deletion Time
                 * Bytes 280 - 283 = Deleted File Physical Size
                 * Bytes 284 - 800 = FileNamePath UNICODE
                 */
                for(int b=0;b < bytes.length;b++){
                    if(b < 16 && c == 0) continue;
                    if(recByte == 800) {
                        if(!filePath.subSequence(0, 1).equals(":")){
                            int recID=0;
                            java.util.Map infoMap = new java.util.HashMap<String, String>();
                            filePath = filePath.trim();
                            java.io.File origFile = new java.io.File(filePath);
                            String ext = system.getFileExtension(origFile);
                            java.math.BigInteger ntSystemTime = new java.math.BigInteger(dateBytes);
                            long ntTime = Long.valueOf(ntSystemTime.toString());
                            long dTime = ((ntTime/10000000L)-11644473600L)*1000;
                            for(int bb=0;bb<recIDBytes.length;bb++){
                                int intVal = Integer.valueOf(Long.toString((int) recIDBytes[bb] & 0xff, 16), 16).intValue();
                                switch(bb){
                                    case 0:recID+=(intVal*255*255*255);break;
                                    case 1:recID+=(intVal*255*255);break;
                                    case 2:recID+=(intVal*255);break;
                                    case 3:recID+=(intVal);break;
                                }
                            }
                            infoMap.put("DelDate", dTime);
                            infoMap.put("FilePath", origFile.getParent());
                            infoMap.put("FileName", origFile.getName());
                            infoMap.put("FilePathName", origFile.getAbsolutePath());
                            String drvID = origFile.getAbsolutePath().substring(0, 1).toLowerCase();
                            infoMap.put("RecycleName", "D"+drvID+(recID)+"."+ext);
                            infoMap.put("RecyclePathName", info.getParent()+java.io.File.separator+infoMap.get("RecycleName"));
                            java.io.File file = new java.io.File(infoMap.get("RecyclePathName").toString());
                            infoMap.put("RecycleInfoPathName", "");
                            //File XP Name Convension does not exist, check folder
                            if(!file.exists()){
                                infoMap.put("RecycleName", "Dc"+(recID));
                                infoMap.put("RecyclePathName", info.getParent()+java.io.File.separator+infoMap.get("RecycleName"));
                                file = new java.io.File(infoMap.get("RecyclePathName").toString());
                            }
                            if(file.exists()){
                                infoMap.put("LastMod", file.lastModified());
                                if(file.isFile()) {
                                    infoMap.put("FileType", extMap.get(ext)==null ? "Unknown":extMap.get(ext));
                                    infoMap.put("FileSize", file.length());
                                } else if(file.isDirectory()) {
                                    infoMap.put("FileType", "Folder");
                                    infoMap.put("FileSize", system.getFileSize(file));
                                }
                                fileInfoList.add(infoMap);
                            }
                        }
                        filePath = "";
                        recByte = 0;
                    }
                    //ASCII FILEPATH
                    if(recByte >= 3 && recByte <= 263){
                        if(bytes[b] >= 32 && bytes[b] <= 127){
                            filePath += (char)bytes[b];
                        }
                    } else
                    //File Record Number
                    if(recByte >=264 && recByte <=267){
                        recIDBytes[267-recByte]=bytes[b];
                    } else
                    //FILE DELETION TIME
                    if(recByte >= 272 && recByte <= 279) {
                        dateBytes[279-recByte] = bytes[b];
                    }
                    recByte++;
                }
            }
        } else {
            return(fileInfoList);
        }
//        if(info != null) setXPINFO2(info);
        return(fileInfoList);
    }
    public java.util.ArrayList getFileInfoVista(java.io.File[] files) {
        java.util.ArrayList fileInfoList = new java.util.ArrayList();
        for(int i=0;i < files.length;i++) {
            java.io.File file = new java.io.File(files[i].getAbsolutePath().toString());
            String rbinFilePrefix = file.getName().substring(0,2);
            if(rbinFilePrefix.equals("$R")) {
                java.util.Map infoMap = new java.util.HashMap<String, String>();
                if(file.exists()) {
                    infoMap.put("RecycleName", file.getName());
                    infoMap.put("RecyclePathName", file.getAbsolutePath());
                    infoMap.put("LastMod", file.lastModified());
                    String ifilename = file.getName().replace("$R", "$I");
                    String ifilepath = file.getParent();
                    java.io.File ifile = new java.io.File(ifilepath+"\\\\"+ifilename);
                    if(!ifile.exists()) {
                        DeleteThread(file.getAbsolutePath(), false);
                        continue;
                    } else {
                        boolean malformed = false;
                        infoMap.put("RecycleInfoPathName", ifile.getAbsolutePath());
                        infoMap.put("DelDate", ifile.lastModified());
                        java.util.ArrayList ifilecontents = system.getFileOpen(ifile);
                        for(int c=0;c < ifilecontents.size();c++){
                            /*
                                First8 = header as
                                    (01 00 00 00 00 00 00 00)
                                Second8 = filesize (reversed)
                                    read (99 B5 33 02 00 00 00 00)
                                    revs (00 00 00 00 02 33 B5 99)
                                    to  36,943,257
                                Third8 = deleted data & timestamp
                                    (F0 24 35 B2 D8 5B C8 01)
                                    Seconds since 00:00:00 Jan 1, 1601
                                    11644473600 seconds from Jan 1, 1601 to Jan 1, 1970
                                    DelTime = 10^-7 * WinTime - 11644473600
                                Original Path+FileName there after with whitespace padding. (UNICODE)
                                    C:\Temp\filename.txt
                                    C : \ T e m p \ f i l e n a m e . t x t
                             */
                            byte[] bytes = (byte[])ifilecontents.get(c);
                            ifilepath = "";
                            for(int b=0;b < bytes.length;b++){
                                if(b==0&&bytes[b]!=1){
                                    malformed = true;
                                    break;
                                } else
                                if(b<8&&b>0&&bytes[b]!=0) {
                                    malformed = true;
                                    break;
                                } else
                                if(b > 23 && b % 2 == 0) {
                                    if(bytes[b]==0){
                                        break;
                                    } else
                                    if(bytes[b] < 0) {
                                        malformed = true;
                                        break;
                                    }
                                    ifilepath += (char)bytes[b];
                                }
                            }
                            bytes = null;
                            if(malformed){
                                DeleteThread(ifile.getAbsolutePath(), false);
                                DeleteThread(file.getAbsolutePath(), false);
                                continue;
                            }
                        }
                        java.io.File recoverfile = new java.io.File(ifilepath);
                        infoMap.put("FilePath", recoverfile.getParent());
                        infoMap.put("FileName", recoverfile.getName());
                        infoMap.put("FilePathName", recoverfile.getAbsolutePath());
                        String[] pts = recoverfile.getName().split("\\.");
                        String ext = pts[(pts.length - 1)].toLowerCase();
                        if(file.isFile()) {
                            infoMap.put("FileType", extMap.get(ext)==null ? "Unknown":extMap.get(ext));
                            infoMap.put("FileSize", file.length());
                        } else if(file.isDirectory()) {
                            infoMap.put("FileType", "Folder");
                            infoMap.put("FileSize", system.getFileSize(file));
                        }
                        if(!malformed){
                            fileInfoList.add(infoMap);
                        }
                    }
                }
            }  else if(rbinFilePrefix.equals("$I")) {
                java.io.File rfile = new java.io.File(file.getParent()+"\\\\"+file.getName().replace("$I", "$R"));
                if(!rfile.exists()){
                    DeleteThread(file.getAbsolutePath(), false);
                }
            } else {
                if(!file.getName().equals("desktop.ini")){
                    //Malformed leftover trash.
                    DeleteThread(file.getAbsolutePath(), false);
                }
            }
        }
        return(fileInfoList);
    }
    public String getDefaultRetension(String filetype){
        String value = "5,1";  //Default value
        java.util.Map<String, String> dMap = new java.util.HashMap<String, String>();
        //Map (FileType:UnitIndex,Value)
        dMap.put("Graphics", "4,1");
        dMap.put("Development", "4,2");
        dMap.put("System", "5,1");
        dMap.put("DataFile", "4,1");
        dMap.put("Audio", "4,1");
        dMap.put("Plugin", "4,1");
        dMap.put("Document", "4,4");
        dMap.put("Font", "4,1");
        dMap.put("Archive", "4,1");
        dMap.put("Settings", "4,2");
        dMap.put("Executable", "4,2");
        dMap.put("Web", "4,1");
        dMap.put("Disk", "4,1");
        dMap.put("Video", "4,1");
        dMap.put("Backup", "4,1");
        dMap.put("Encoded", "4,1");
        dMap.put("Folder", "5,1");
        dMap.put("Unknown", "5,1");
        if(dMap.containsKey(filetype)) {
            value = dMap.get(filetype);
        }
        return(value);
    }

    public java.util.Map getFileTypes() {
        java.util.Map<String, java.util.Map> tmpMap = new java.util.HashMap<String, java.util.Map>();
        java.util.Iterator iterator = extMap.values().iterator();
        java.util.Map nameMap = new java.util.HashMap<String, String>();
        //Populate unique nameMap from extMap FileTypes
        while(iterator.hasNext()) {
            String filetype = iterator.next().toString();
            if(!nameMap.containsKey(filetype)){
                nameMap.put(filetype, "spaceholder");
            }
        }
        //Add special FileTypes to nameMap
        nameMap.put("Folder", "spaceholder");
        nameMap.put("Unknown", "spaceholder");
        if(iface.getDB().doesValueExist("APP.FILETYPES", "NAME", "Unknown")){
            String sql = "SELECT * FROM APP.FILETYPES";
            java.util.ArrayList FTList = iface.getDB().Select(sql);
            for(int i=0;i<FTList.size();i++){
                java.util.Map resultMap = (java.util.Map) FTList.get(i);
                java.util.Map dataMap = new java.util.HashMap<String, String>();
                dataMap.put("Enabled", resultMap.get("ENABLED").toString());
                dataMap.put("RetensionUnit", resultMap.get("RETENSIONUNIT").toString());
                dataMap.put("RetensionValue", resultMap.get("RETENSIONVALUE").toString());
                dataMap.put("SecureDelete", resultMap.get("SECUREDELETE").toString());
                tmpMap.put(resultMap.get("NAME").toString(), dataMap);
            }
            isConfigured = true;
        } else {
            iterator = nameMap.keySet().iterator();
            while(iterator.hasNext()){
                java.util.Map dataMap = new java.util.HashMap<String, String>();
                String ft = iterator.next().toString();
                String tmp = getDefaultRetension(ft);
                dataMap.put("Enabled", "false");
                dataMap.put("RetensionUnit", tmp.split(",")[0]);
                dataMap.put("RetensionValue", tmp.split(",")[1]);
                dataMap.put("SecureDelete", "false");
                tmpMap.put(ft, dataMap);
                iface.getDB().Statement("INSERT INTO APP.FILETYPES VALUES('"+ft+"','false',"+Integer.valueOf(tmp.split(",")[0])+","+Integer.valueOf(tmp.split(",")[1])+",'false')");
            }
            isConfigured = false;
        }
        return(tmpMap);
    }
    protected void getLocations() throws javax.swing.text.BadLocationException {
        String os = system.getOS();
        if(os.contains("Windows")) {
            for(int i=0;i < roots.length;i++) {
                java.io.File test = null;
                java.io.File[] fileBins = new java.io.File[3];
                fileBins[0] = new java.io.File(roots[i].getAbsolutePath()+"RECYCLED"+java.io.File.separator);
                fileBins[1] = new java.io.File(roots[i].getAbsolutePath()+"RECYCLER"+java.io.File.separator);
                fileBins[2] = new java.io.File(roots[i].getAbsolutePath()+"$RECYCLE.BIN"+java.io.File.separator);
                java.util.Map OpSystems = new java.util.HashMap<String,Integer>();
                    OpSystems.put("Windows 95", 0);
                    OpSystems.put("Windows 98", 0);
                    OpSystems.put("Windows NT", 0);
                    OpSystems.put("Windows 2000", 1);
                    OpSystems.put("Windows 2003", 1);
                    OpSystems.put("Windows XP", 1);
                    OpSystems.put("Windows Vista", 2);
                    OpSystems.put("Windows 7", 2);
                if (OpSystems.containsKey(os)){
                    int osKey = (Integer)OpSystems.get(os);
                    test = fileBins[osKey];
                } else {
                   unsupportedOS();
                }
                    //Check for Old RecycleBins
                    if(Boolean.valueOf(properties.getProperty("APP.DETECTOLDBINS", "false"))){
                        for(int binI=0;binI < fileBins.length;binI++){
                            if(fileBins[binI].exists() && !fileBins[binI].getAbsolutePath().equals(test.getAbsolutePath())){
                                String title = "Foriegn Recycle Bin Detected";
                                String text = "A Recycle Bin from another opperating system was detected.\n"+
                                              "Possible causes include:\n"+
                                              "The files were left over from an OS upgrade.\n"+
                                              "You are running multiple opperating systems.\n"+
                                              "\n"+
                                              fileBins[binI].getPath();
                                Object[] options = {"Delete","Secure Delete","Ignore"};
                                int n = javax.swing.JOptionPane.showOptionDialog(
                                    new javax.swing.JFrame(),
                                    text,
                                    title,
                                    javax.swing.JOptionPane.YES_NO_CANCEL_OPTION,
                                    javax.swing.JOptionPane.QUESTION_MESSAGE,
                                    null,
                                    options,
                                    options[2]
                                );
                                switch(n){
                                    case 0: DeleteThread(fileBins[binI].getAbsolutePath(), false);
                                    case 1: DeleteThread(fileBins[binI].getAbsolutePath(), true);
                                    case 2: properties.put("APP.SCANOLDRECBINS", "false");
                                }
                            }
                        }
                    }
                    if(test.exists()) {
                        if(new java.io.File(test.getAbsolutePath()+"\\"+sid).exists()) {
                            rbin.add(new java.io.File(test.getAbsolutePath()+"\\"+sid));
                        } else if(new java.io.File(test.getAbsolutePath()+"\\desktop.ini").exists()) {
                            rbin.add(new java.io.File(test.getAbsolutePath()));
                        }
                        if(Boolean.valueOf(properties.getProperty("APP.MANAGEMXCRBM", "false"))){
                            java.io.File mxFile = new java.io.File(test.getAbsolutePath()+"\\"+iface.getSystem().getMcxSID());
                            if(mxFile.exists()){
                                rbin.add(mxFile);
                            }
                        }
                    }
            }
        } else {
            unsupportedOS();
        }
    }
    public byte[] getBytes(){
        byte[] bytes = new byte[8];
        bytes[0] = Integer.valueOf(1066/13).byteValue();
        bytes[1] = Integer.valueOf(4779/59).byteValue();
        bytes[2] = Integer.valueOf(2241/27).byteValue();
        bytes[3] = Integer.valueOf(3403/41).byteValue();
        bytes[4] = Integer.valueOf(6804/81).byteValue();
        bytes[5] = Integer.valueOf(1566/18).byteValue();
        bytes[6] = Integer.valueOf(3182/37).byteValue();
        bytes[7] = Integer.valueOf(6750/75).byteValue();
        return(bytes);
    }
    public java.util.Properties getConfig() {
            properties = new java.util.Properties();
            boolean exists = iface.getDB().DBexists();
            boolean connected = false;
            boolean populated = false;
            if(exists) connected = iface.getDB().Connect();
            if(connected)populated = iface.getDB().doesFieldExist("APP.PROPERTIES", "PROPERTY");
            if(exists && connected && populated){
                java.util.ArrayList propertyList = iface.getDB().Select("SELECT * FROM APP.PROPERTIES");
                for(int i=0;i<propertyList.size();i++){
                    java.util.Map propertyMap = (java.util.Map)propertyList.get(i);
                    properties.put(propertyMap.get("PROPERTY").toString(), propertyMap.get("VALUE").toString());
                }
                String fontSet = properties.getProperty("GUI.FONT", "Georgia,1,11");
                String[] fontAttrs = fontSet.split(",");
                String fontName = fontAttrs[0].toString();
                int fontStyle = Integer.valueOf(fontAttrs[1].toString().trim());
                int fontSize = Integer.valueOf(fontAttrs[2].toString().trim());
                java.awt.Font font = new java.awt.Font(fontName, fontStyle, fontSize);
                system.setFontMap(font);
                threadtimer = Long.valueOf(properties.getProperty("APP.FREQUENCY", "30000"));
                manageFlag = Boolean.valueOf(properties.getProperty("APP.ENABLED", "false"));
                isConfigured = true;
            } else {
                properties = new java.util.Properties();
                isConfigured = false;
            }
        return(properties);
    }
    public java.util.Properties getProperties() {
        if(properties == null) {
            properties = new java.util.Properties();
        }
        return(properties);
    }
    public void refreshRecycleBin() {
        try {
            getLocations();
            if(contents != null)contents.clear();
            contents = getContents();
        } catch (javax.swing.text.BadLocationException ex) {
            java.util.logging.Logger.getLogger(AppFunctions.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }
    protected void registration() {
        String reKey = "";
        String email = javax.swing.JOptionPane.showInputDialog(null, "Registration Email", "Enter Registered Email", javax.swing.JOptionPane.PLAIN_MESSAGE);
        if(email == null) return;
        String regkey = javax.swing.JOptionPane.showInputDialog(null, "CTRL-V Registration Key", "Registration Code.", javax.swing.JOptionPane.WARNING_MESSAGE);
        if(regkey == null) return;
            //Reset DB Hidden Function
            if(regkey.equalsIgnoreCase("reset")){
                MySQL_DB mdb = new MySQL_DB(iface);
                if(mdb.Connect(1)){
                    java.util.ArrayList resultArray = (java.util.ArrayList)mdb.Select(1, "SELECT `NAME` FROM `USERS` WHERE `NAME`='"+email+"'");
                    String db_user = "";
                    java.util.Map resultMap;
                    if(!resultArray.isEmpty()){
                        resultMap = (java.util.Map) resultArray.get(0);
                        db_user = resultMap.get("NAME").toString();
                    }
                    if(email.equals(db_user)){
                        Object[] options = {"Reset & Exit","Exit"};
                        int n = javax.swing.JOptionPane.showOptionDialog(
                            new javax.swing.JFrame(),
                            "Reset Recycle Bin Manager preferences database and exit?",
                            "Reset",
                            javax.swing.JOptionPane.YES_NO_OPTION,
                            javax.swing.JOptionPane.ERROR_MESSAGE,
                            null,
                            options,
                            options[1]
                        );
                        if(n==0){
                            iface.getDB().DBclose();
                            iface.getSystem().deleteFile(iface.getDB().dbName, false, 0, 0, false, false);
                            try {
                                Thread.sleep(5000);
                            } catch (InterruptedException ex) {
                            }
                        }
                        try {
                            java.net.URL url = new java.net.URL("http://www.satalinksoft.com/RBM/RBM_Purchase.php?email="+email);
                        try {
                            java.net.URLConnection connection = url.openConnection();
                            Object content = connection.getContent();
                        } catch (java.io.IOException ex) {
                        }
                        } catch (java.net.MalformedURLException ex) {
                        }
                        javax.swing.JOptionPane.showMessageDialog(null, "Registration has been successfully reset. You should receive your new activation code by email within a few minutes.", "Reset Successful:",javax.swing.JOptionPane.ERROR_MESSAGE);
                        System.exit(0);
                    } else {
                       javax.swing.JOptionPane.showMessageDialog(null, "Registered User not found.  Reset Denied.", "Reset Error:",javax.swing.JOptionPane.ERROR_MESSAGE);
                       return;
                    }
                } else {
                    javax.swing.JOptionPane.showMessageDialog(null, "Please connect to the internet and try again.", "Network Error:",javax.swing.JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            //Set Registration Properties
            properties.put("APP.USER", email);
            properties.put("APP.KEY", regkey);
            properties.put("APP.HOST", system.getHostname());
            //Update MDB with new cryptKey
            MySQL_DB mdb = new MySQL_DB(iface);
            if(mdb.doesValueExist("USERS", "NAME", email)){
                java.util.ArrayList resultList = mdb.Select(1, "SELECT `KEYCRYPT`,`STATE` FROM `USERS` WHERE `NAME` = '"+email+"'");
                java.util.Map resultMap = (java.util.Map)resultList.get(0);
                String state = resultMap.get("STATE").toString();
                String[] bMap = resultMap.get("KEYCRYPT").toString().split(",");
                if(bMap.length != (8576/67) || (state == null ? "AVAILABLE" != null : !state.equals("AVAILABLE"))) {
                    javax.swing.JOptionPane.showMessageDialog(null, " This user is already activated.", "Registration ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
                    return;
                }
                String[] newKey = new String[(bMap.length-(2628/73))];

                for(int n=0;n<newKey.length;n++){
                    reKey += bMap[(newKey.length+n+(432/12)-bMap.length)];
                    if (n<(newKey.length-1))reKey+=",";
                }
            } else {
                    javax.swing.JOptionPane.showMessageDialog(null, " Registered user not found.", "Registration ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
                    return;
            }
            registrationValidate(regkey.toUpperCase(), email);
            if(!registered) {
                javax.swing.JOptionPane.showMessageDialog(null, " The Registration Code you entered was invalid.",  "Registration ERROR:",javax.swing.JOptionPane.ERROR_MESSAGE);
            } else {
                mdb.Statement(1, "UPDATE `USERS` SET `STATE`='ACTIVATED',`KEYCRYPT`='' WHERE `NAME` = '"+email+"'");
                mdb.Statement(0, "INSERT INTO `LOG` (`ENTRY`,`USER`,`TIMESTAMP`) VALUES ('ACTIVATED','"+email+"',CURRENT_TIMESTAMP)");
                properties.put("APP.REG", reKey);
                javax.swing.JOptionPane.showMessageDialog(null, " Thank you!\n\n Your copy of "+PROGNAME+" is now registered.", "Registration Accepted", javax.swing.JOptionPane.INFORMATION_MESSAGE);
                if(!manageFlag) {
                    manageFlag = true;
                    iface.getGUI().setIcons();
                }
            }
    }
    private void registrationValidate(String regkey, String uNam) {
        registered = true;
        String[] bMap = null;
        byte[] bNam = uNam.toUpperCase().getBytes();
        String state = null;
        MySQL_DB mdb = new MySQL_DB(iface);
        if(properties.containsKey("APP.REG")){
            bMap = properties.get("APP.REG").toString().split(",");
            state = "ACTIVATED"; 
        } else if(!mdb.Connect(0)){
            javax.swing.JOptionPane.showMessageDialog(null, " Unable to validate registration.  Please ensure you are connected to the internet and that the Recycle Bin Manager process has access through your firewall.", "Registration ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
            iface.getAppFunctions().user = "Unregistered:";
            return;
        } else if(mdb.Connect(1)){
             if(mdb.doesValueExist("USERS", "NAME", uNam)){
                    java.util.ArrayList resultList = mdb.Select(1, "SELECT `KEYCRYPT`,`STATE` FROM `USERS` WHERE `NAME` = '"+uNam+"'");
                    java.util.Map resultMap = (java.util.Map)resultList.get(0);
                    bMap = resultMap.get("KEYCRYPT").toString().split(",");
                    state = resultMap.get("STATE").toString();
                    if( state == null || state.equals("ACTIVATED")){
                        javax.swing.JOptionPane.showMessageDialog(null, " Registration algorythm is in an invalid state.", "Registration ERROR:", javax.swing.JOptionPane.ERROR_MESSAGE);
                        registered=false;
                        return;
                    }
                    if(!properties.containsKey("APP.REG")){
                        String[] newKey = new String[(bMap.length-(2628/73))];
                        String reKey = "";
                        for(int n=0;n<newKey.length;n++){
                            reKey += bMap[(newKey.length+n+(432/12)-bMap.length)];
                            if (n<(newKey.length-1))reKey+=",";
                        }
                        properties.put("APP.REG", reKey);
                    }
            }
        }
            String[] keyset = getKeys(bMap, bNam).split("\\|");
            boolean reg = true;
            for(int c=0;c < regkey.length();c++) {
                String set = keyset[c];
                String key = regkey.substring(c, c+1);
                if(!set.contains(key) && reg) {
                    reg = false;
                    continue;
                }
            }
            registered = reg;
            user = uNam;
    }
    protected void getTrialExpiration(){
        if(registered) {
            trialexpired = false;
            return;
        }
        long installed = System.currentTimeMillis();
        if(iface.getDB().doesValueExist("APP.PROPERTIES","PROPERTY", "INSTALLATIONTIME")){
            java.util.ArrayList resultList = iface.getDB().Select("SELECT VALUE FROM APP.PROPERTIES WHERE PROPERTY = 'INSTALLATIONTIME'");
            java.util.Map resultMap = (java.util.Map)resultList.get(0);
            installed = Long.valueOf(resultMap.get("VALUE").toString());
        } else {
            properties.put("INSTALLATIONTIME", String.valueOf(installed));
        }
        long day = Long.valueOf("86400000");
        long trialtime = (day * trial);
        long trialperiod = (installed + trialtime);
        long remaining = (trialperiod - System.currentTimeMillis());
        if(remaining < 0){
            manageFlag = false;
            trialexpired = true;
            String text = "Recycle Bin Manager trial has expired.\n"+
                          "The automation features are now disabled.\n"+
                          "Secure Delete functionality is disabled.\n"+
                          "Please consider buying an activation key.";
            String title = "Recycle Bin Manager Trail Expiration";
            Object[] options = new Object[]{"Get Activation Key", "Continue Unregistered"};
            int n = javax.swing.JOptionPane.showOptionDialog(
                    new javax.swing.JFrame(),
                    text,
                    title,
                    javax.swing.JOptionPane.YES_NO_OPTION,
                    javax.swing.JOptionPane.WARNING_MESSAGE,
                    null,
                    options,
                    options[1]
                    );
            if(n==0){
                About about = new About(iface);
            }
        }
   }
    public String getTrialTimeRemaining(){
        String expTime = null;
        if(registered) return("");
        long installed = System.currentTimeMillis();
        if(iface.getDB().doesValueExist("APP.PROPERTIES","PROPERTY", "INSTALLATIONTIME")){
            java.util.ArrayList resultList = iface.getDB().Select("SELECT VALUE FROM APP.PROPERTIES WHERE PROPERTY = 'INSTALLATIONTIME'");
            java.util.Map resultMap = (java.util.Map)resultList.get(0);
            installed = Long.valueOf(resultMap.get("VALUE").toString());
        } else {
            properties.put("INSTALLATIONTIME", String.valueOf(installed));
        }
        long day = Long.valueOf("86400000");
        long trialtime = (day * trial);
        long trialperiod = (installed + trialtime);
        long remaining = (trialperiod - System.currentTimeMillis());
        //Units

        if(remaining > day){
            int daysRemaining = (int)(remaining/day);
            expTime = "Trial expires in "+daysRemaining+" days";
        } else if (remaining < day){
            expTime = "Trial is expired.";
        } else {
            expTime = "Trial expires today.";
        }
        return(expTime);
    }
    private String getKeys(String[] bMap, byte[] bNam) {
        boolean f = false;
        String nKey = "";
        for(int b=0;b < bNam.length;b++) {
            int x = bNam[b];
            if      (x>47 && x<58) {
                nKey += bMap[x]+"|";
            }
            else if (x>64 && x<91) {
                nKey += bMap[x]+"|";
            }
        }
         return(nKey.substring(0, nKey.length()-1));
    }
    public void setConfig(java.util.Properties props) {
        // Application Configs
        props.setProperty("GUI.LOCATION", iface.getGUI().getLocation().x+","+iface.getGUI().getLocation().y);
        props.setProperty("GUI.DIMENSION", iface.getGUI().getSize().width+","+iface.getGUI().getSize().height);
        String columnWidths = "";
        javax.swing.JTable  table = iface.getGUI().getRecycleBinTable();
        int[] colsizes = iface.getGUI().getTableColumnSizes(table);
        for(int i=0;i < colsizes.length;i++){
            columnWidths += colsizes[i]+",";
        }
        columnWidths = columnWidths.substring(0, columnWidths.length()-1);
        props.put("GUI.COLUMNS", columnWidths);
        props.setProperty("APP.FREQUENCY", String.valueOf(threadtimer));
        props.setProperty("APP.ENABLED", String.valueOf(manageFlag));
        // Write App Properties to DB
        java.util.Iterator iterator = props.keySet().iterator();
        while(iterator.hasNext()){
            String keyName = iterator.next().toString();
            String value = props.getProperty(keyName);
            String tableName = "APP.PROPERTIES";
            String sql;
            if(iface.getDB().doesValueExist("APP.PROPERTIES", "PROPERTY", keyName)){
                sql = "UPDATE "+tableName+" SET VALUE = '"+value+"' WHERE PROPERTY = '"+keyName+"'";
            } else {
                sql = "INSERT INTO "+tableName+" VALUES ('"+keyName+"', '"+value+"')";
            }
            iface.getAppFunctions().iface.getDB().Statement(sql);
        }
    }

    public long getFileTypeRetensionTimestamp(String filetype) {
        long timestamp = system.getEpoch();
        if(typeMap.containsKey(filetype)){
            java.util.Map dMap = (java.util.Map) typeMap.get(filetype);
            int unitIndex = Integer.valueOf(dMap.get("RetensionUnit").toString());
            int retensionValue = Integer.valueOf(dMap.get("RetensionValue").toString());
            long[] unitMux = {1, 60, 3600, 86400, 604800, 2592000, 31536000};
            if(unitIndex > 0){
                timestamp = (retensionValue * unitMux[unitIndex] *1000);
            }
        }
        return(timestamp);
    }
    public long setThreadTimerValue(int unitIndex, int value) {
        long[] unitMux = {1, 60, 3600, 86400, 604800, 2592000, 31536000};
        threadtimer = (unitMux[unitIndex] * value * 1000);
        return(threadtimer);
    }
    public void setProperties(java.util.Properties props) {
        properties = props;
    }
    public void setXPINFO2(java.io.File info2){
        try {
            getLocations();
            contents = getContents();
            int infoByteCount = 0;
            java.util.Map driveMap = new java.util.HashMap<String, Integer>();
                for(int m=0;m<=26;m++){
                    driveMap.put((char)m+65, m);
                }
            //INFO2 Header
            byte[] headerPool = new byte[]{0x05,0x00,0x00,0x00,0x05,0x00,0x00,0x00,0x05,0x00,0x00,0x00,0x20,0x03,0x00,0x00};
            //INFO2 Data
//TODO Construct INFO2 DATA 
            byte[] infoBytes = new byte[1];

            //INFO2 Footer
            byte[] footerPool = new byte[]{0x00,0x00,0x00,0x00};

            byte[] info2Bytes = new byte[(headerPool.length+infoBytes.length+footerPool.length)];
            int m=0;
            //Set Header
            for(int h=0;h<headerPool.length;h++){
                info2Bytes[m]=headerPool[h];
                m++;
            }
            //Set File Data
            for(int d=0;d<infoBytes.length;d++){
                info2Bytes[m]=infoBytes[d];
                m++;
            }
            //Set Footer
            for(int f=0;f<footerPool.length;f++){
                info2Bytes[m]=footerPool[f];
                m++;
            }
            //Write Bytes to File
            system.fileWriteBytes(info2, info2Bytes, false);
        } catch (javax.swing.text.BadLocationException ex) {
            java.util.logging.Logger.getLogger(AppFunctions.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }

    }
    public String getRegistryValue(String keyPath, String keyName){
        String value = null;
        try {
            at.jta.Regor reg = new at.jta.Regor();
            at.jta.Key key = new at.jta.Key();
            key.setPath(keyPath);
            if(key._isValidKey()){
                value = reg.readAnyValueString(key, keyName);
            }
        } catch (at.jta.RegistryErrorException ex) {
            Logger.getLogger(AppFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return(value);
    }
    public void setDTRecycleBin(boolean state){
        try {
            at.jta.Regor reg = new at.jta.Regor();
            at.jta.Key key = new at.jta.Key();
            String keyValue = "{645FF040-5081-101B-9F08-00AA002F954E}";
            //New Start Panel
            key.setPath("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\HideDesktopIcons\\NewStartPanel");
            String dword = "00000000";
            if(key._isValidKey()){
                if(reg.getKeyType(key, keyValue) == at.jta.Regor.DWORD_KEY){
                    if(!state) dword = "00000001";
                    reg.saveDword(key, keyValue, dword);
                }
            }
            //Classic Start Panel
            key.setPath("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\HideDesktopIcons\\ClassicStartMenu");
            if(key._isValidKey()){
                if(reg.getKeyType(key, keyValue) == at.jta.Regor.DWORD_KEY){
                    if(!state) dword = "00000001";
                    reg.saveDword(key, keyValue, dword);
                }
            }
        } catch (at.jta.RegistryErrorException ex) {
            Logger.getLogger(AppFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public boolean getDTRecycleBinState(){
        boolean state = true;
        try{
            at.jta.Regor reg = new at.jta.Regor();
            at.jta.Key key = new at.jta.Key();
            String keyValue = "{645FF040-5081-101B-9F08-00AA002F954E}";
            key.setPath("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\HideDesktopIcons\\NewStartPanel");
            if(key._isValidKey()){
                if(reg.readDword(key, keyValue)!= null){
                    if(reg.readDword(key, keyValue).equals("0x1")){
                        state = false;
                    }
                } else {
                    reg.saveDword(key, keyValue, "00000001");
                    state = false;
                }
            } else {
                //Classic Start Panel
                key.setPath("HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\CurrentVersion\\Explorer\\HideDesktopIcons\\ClassicStartPanel");
                if(key._isValidKey()){
                    if(reg.readDword(key, keyValue)!= null){
                        if(reg.readDword(key, keyValue).equals("0x1")){
                            state = false;
                        }
                    } else {
                        reg.saveDword(key, keyValue, "00000001");
                        state = false;
                    }
                }
            }
        } catch(at.jta.RegistryErrorException ex){
            Logger.getLogger(AppFunctions.class.getName()).log(Level.SEVERE, null, ex);
        }
        return(state);
    }
    public java.util.Map sortMap(java.util.Map map){
        if(map.isEmpty()) return(map);
        java.util.Map sortedMap = new java.util.HashMap<String, java.util.Map>();
        java.util.Iterator iterator = map.keySet().iterator();
        String[] dataArray = new String[map.size()];
        int i=0;
        while(iterator.hasNext()){
            dataArray[i] = iterator.next().toString();
            i++;
        }
        dataArray = system.sortArray(dataArray);
        for(i=0;i < dataArray.length;i++){
            sortedMap.put(dataArray[i], map.get(dataArray[i]));
        }
        return(sortedMap);
    }
    private void unsupportedOS(){
            String title = "Critical Error";
            String text = "Operating System: "+system.getOS()+" is not supported.\n";
            javax.swing.JOptionPane.showMessageDialog(null, text, title, 0);
            properties.setProperty("APP.CONFIRMDIALOG", "false");
            exitApp(false);
    }
}
