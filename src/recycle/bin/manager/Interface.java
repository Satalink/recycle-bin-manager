package recycle.bin.manager;

public interface Interface {

    public AppFunctions getAppFunctions();
    public DB getDB();
    public GUI getGUI();
    public Sys getSystem();
}
